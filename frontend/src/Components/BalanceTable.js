import React, { Component } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';

const columns = [{
  dataField: 'name',
  text: 'Exchange',
  sort: true,
  filter: textFilter()
}, {
  dataField: 'free',
  text: 'Free',
  sort: true
}, {
  dataField: 'used',
  text: 'Used',
  sort: true
}];


class BalanceTable extends Component {
    constructor(props) {
        super(props);
        this.state = {data: ["hello", "hello2"]};
      }
      componentDidMount(){
        fetch('http://localhost:8080/api/balances')
        .then(response => response.json())
        .then((results) => {
            results.forEach(
                function(element) {
                    console.log(element)
                    element.free = JSON.stringify(element.free)
                    element.used = JSON.stringify(element.used)
                  return element
                });
          this.setState({ data: results });
          console.log(results)})
        .catch(error => console.log(error))
      }
    render() {
    return (
        //<p> {this.state.data} </p>
        <BootstrapTable keyField='id' data={ this.state.data } columns={ columns } />
    );
  }
}

export default BalanceTable;
