import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import FeeTable from './Components/FeeTable.js'
import BalanceTable from './Components/BalanceTable.js'
import '../node_modules/react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';


class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to Mercury</h1>
        </header>
        
        <Tabs>
    <TabList>
      <Tab>Fees</Tab>
      <Tab>Open Loops</Tab>
      <Tab>Closed Loops</Tab>
      <Tab>Orders</Tab>
      <Tab>Balances</Tab>
    </TabList>
    <TabPanel>
      <FeeTable></FeeTable>
    </TabPanel>
    <TabPanel>
      <h2>Any content 2</h2>
    </TabPanel>
    <TabPanel>
      <h2>Any content 3</h2>
    </TabPanel>
    <TabPanel>
      <h2>Any content Orders</h2>
    </TabPanel>
    <TabPanel>
      <BalanceTable></BalanceTable>
    </TabPanel>
  </Tabs>
      </div>
    );
  }
}

export default App;
