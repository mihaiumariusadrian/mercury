'use strict';

var mercury = require("./mercury")

exports.createOrder = function(args, res, next) {
  /**
   * send buy order
   * Send market buy order, define exchange, symbol
   *
   * order Order  (optional)
   * returns ApiResponse
   **/
  var examples = {};
  examples['application/json'] = {
  "code" : 0,
  "type" : "aeiou",
  "message" : "aeiou"
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.getBalances = async function(args, res, next) {
  /**
   * Get balances
   *
   * returns List
   **/
  let balances = await (mercury.getBalances())
  if (Object.keys(balances).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(balances || {}, null, 2));
  } else {
    res.end();
  }
}

exports.getFees = function(args, res, next) {
  /**
   * Get fees
   *
   * returns List
   **/
  var fees = mercury.getFees()
  if (Object.keys(fees).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(fees || {}, null, 2));
  } else {
    res.end();
  }
}

exports.getTickers = function(args, res, next) {
  /**
   * Get Tickers
   * Get all tickers
   *
   * returns List
   **/

   let tickers = mercury.getTickers()
 
  if (Object.keys(tickers).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(tickers || {}, null, 2));
  } else {
    res.end();
  }
}

