'use strict'

console.log("mercury start")


var ccxt = require('ccxt')

var baseFiat = "EUR";
var fiats = ["USD", "AUD", "EUR", "ZAR", "IDR", "GBP", "CAD", "JPY", "CZK", "ARS", "MYR", "HKD"];
var baseCryptocurrency = "BTC";
var symbols = ["BTC", "ETH", "BCH","LTC", "XRP"]; //List of accepted cryptocurrencies
var refreshRate = 500; //in seconds
var gainThreshold = 1.0; //Was 1.03


var fees = [
    {name:"Bitstamp", operation: "ALL", fee:"1.0024"},
    {name:"BTC Markets", operation:"ALL", fee:"1.006"},
    {name:"CoinSpot", operation:"ALL", fee:"1.009"},
    {name:"luno", operation: "ALL", fee:"1.005"}, //LUNO HAS 0% MAKER FEE AND 0.20% TO 1% TAKER FEE
    {name:"CoinMate", operation:"ALL", fee:"1.0007"}, //COINMATE HAS A VARIABLE FEE POLICY
    {name:"Kraken", operation: "ALL", fee: "1.00"}, //KRAKEN has no fees until  31/01/2018
    {name:"ACX", operation:"ALL", fee:"1.00"}, //ACX has no fees until the 02/02/2018
    {name:"CoinBase", operation:"ALL", fee:"1.0149"}, //In the site they say its 1.49% but in practice I also think there is a minimum
    {name:"Gatecoin", operation:"ALL", fee:"1.0014"} //Gatecoin has different fee for Maker and Taker. I enter here the highest. 
   ];

var getExchanges = function(){
   
    var exchanges = []
    
            /*******************
        *   ADD EXCHANGES
        *******************/ 
           
        //The Following API has permissions to operaton en the API subaccount of Bitstamp
        //const bitstamp = new ccxt.bitstamp();
        const bitstamp = new ccxt.bitstamp(
            {
            //key limited to IP of server 
            apiKey: 'RiH1HkcxeehmJGqpBuU2E6AbhU0Xo5ox',
            secret: 'aeKWV0fxqJJHqFRjnzK7TmmLWeAfV1qw',
            
            /*Key not limited to the IP of the server
            apiKey: 'Wz3ZMCCK5tIFaqp5hCMXWiByXsKjGfVY',
            secret: 'vJZqND9tuQ0N83ALpSXbnvDPvATvdhXM',
            */
             uid: '887892'
            });
    exchanges.push(bitstamp);
    
    //BTCMARKETS API has FULL UNLIMITED ACCESS. Be very careful operating.
    const btcmarkets = new ccxt.btcmarkets({
                apiKey: '8ec0a553-ffc0-47ce-a788-cd8760a994dc',
                secret: 'tcIVRE6dnMo+JxeL3Z+uSeGw6ZYXyjtzfnwN/ySZneGgGpe4lsUeEHv8sHsE7CbupOA985MysZdmirGo5LY7vg=='
            });
    exchanges.push(btcmarkets);
    
    const acxio = new ccxt.acx({
    apiKey: 'NpTkA1J9xo3wqHRsmaI77Ck9U80xX8DxRPzjLdFG',
    secret: 'XJQkaLoiKv54x1NBDx9y5dsCYhePjcKgwAiDUpcF'
    });
    exchanges.push(acxio);
    
    const kraken = new ccxt.kraken(
        {//key descriptiion in kraken.com "1 FIRST API"
            apiKey: '2IVpyFjA9tb7Yi9ListAEcHdxZiPc6Vxa5Ute+Pz/ux1YZMtI5ri/+v6',
            secret: '9YMq6vGvdQYQgMeoxf7SM5DW8q2pu6OoAjWcb7/bwJB5VBlL+RJs1bbRbBn+2N2grcu3P2n5CV9daNDhUoWPKw=='
        }
    );
    //exchanges.push(kraken);
    
    const luno = new ccxt.luno({
    //key ony active as read-only. No trading or withdrawal
    //Limited to the IP of the server
    //apiKey: 'dr9gtg2sxt9xz',
    //secret:'zFHeFoduhVuIJzfWxQS1f0em-mNJGpA-nxd09xin8gw'
    });
    exchanges.push(luno);
    
    
    const coinspot = new ccxt.coinspot(
        {
            apiKey: 'e65e91e00aef672babe6622f6f6200b7',
            secret: 'Z7YG78WNL4QX723B5NGAJF966803Q2RRQL34CM957GF0GNM3F4ADLNUWDDQAD0TRC0D1M95J6Z1EGK1VD'
        }
    )
    //exchanges.push(coinspot);
    
    const coinmate = new ccxt.coinmate({//Coinmate key not enabled for Withdrawal or Trade. Only account info
            //Key limited to the IP of the server
            apiKey: 'eMEZzsF2Pc4jYkwoTtwXeFtQmban9jdgT4gLr4MQWDQ',
            secret: '9ZJJ3ZkKXZ0AWWxKeqyDwXIA-bcXcxOacgiDSgmuupI',
            uid: ' 38117'
    });
    exchanges.push(coinmate);
    
    const gatecoin = new ccxt.gatecoin(); //Exchange in Hong Kong
    exchanges.push(gatecoin);
    
    //OTHER PENDING EXCHANGES FOR THE FUTURE
    //SatoshiTango doesnt seem to want to load, so this disabled
    //const satoshitango = new ccxt.satoshitango();  //SATOSHITANGO does seem to have the option to create an API
    //exchanges.push(satoshitango);
    //const gdax = new ccxt.gdax()
    //exchanges.push(gdax)
    //const bitfinex = new ccxt.bitfinex ();
    //exchanges.push(bitfinex);
    //const bitbay = new ccxt.bitbay();
    //exchanges.push(bitbay);
    /*COINBASE IS CURRENTLY NOS SUPPORTED BY CCXT. Watiting for implementation
    const coinbase = new ccxt.coinbase({ //Coinbase key has UNLIMITED access to the account.
            //Key limited to the IP of the server
    apiKey: 'Ew6kF0zQugCOfhjA',
    secret: 'pgs9i9NpYLaLVsVutxnAb5KMxTsiAwO4'
    });
    exchange.push(coinbase);
    */
    
    // LOAD MARKETS
    for (var i=0;i<exchanges.length;i++){
        //Load available Markets in the broker
        exchanges[i].loadMarkets().catch((error)=>{return error})
    }
    
    return exchanges
    
}

var exchanges = getExchanges()

var refreshRate = 500; //in seconds

var fiatData ={"base":"EUR","date":"2018-01-25","rates":{"AUD":1.5358,"BGN":1.9558,"BRL":3.9063,"CAD":1.5289,"CHF":1.168,"CNY":7.8549,"CZK":25.381,"DKK":7.4444,"GBP":0.87038,"HKD":9.6996,"HRK":7.4245,"HUF":309.35,"IDR":16493.0,"ILS":4.2229,"INR":78.865,"JPY":135.12,"KRW":1314.4,"MXN":22.881,"MYR":4.8207,"NOK":9.5858,"NZD":1.6823,"PHP":63.189,"PLN":4.1469,"RON":4.6693,"RUB":69.282,"SEK":9.8188,"SGD":1.6202,"THB":38.983,"TRY":4.641,"USD":1.2407,"ZAR":14.761}}


function fiatRate(base, to){
    if(base==to) return 1
    return fiatData.rates[to]
}

async function fetchData(exchanges){
    var tickers= [];
    for (let exchange of exchanges){ //For each excahnge

         //For the moment this gets the standard fee of operation "all" from the specific broker
         let applicablefee;
         //TRY TO GET THE FEE O THE BROKER. If I cant find it use value 1 (0% fees)
         try {
            applicablefee = ourfees.find(o => o.name === exchange.name && o.operation ==='ALL').fee;
            console.log("Loading exchange "+exchange.name+". Applicable Fee: "+applicablefee);
         }
          catch(err) {
            applicablefee = 1;
            console.log("Loading exchange "+exchange.name+" APPLICABLE FEE NOT FOUND. USING VALUE 1.00 (0% FEE)");
            //fee_exchange: 1
          }

        let listOfSymbols = exchange.symbols;
        console.log("List of Symbols: "+listOfSymbols);
        if(listOfSymbols!= null){
             //For each symbol in each exchange...
            for(var i=0; i<listOfSymbols.length; i++){
                let ticker = await (exchange.fetchTicker(listOfSymbols[i])).then(
                    (ticker) => {
                        let customTicker = {
                            exchange: exchange.name,
                            symbol: ticker.symbol,
                            symbol1: ticker.symbol.split('/')[0],
                            symbol2: ticker.symbol.split('/')[1],
                            last: ticker.last,
                            bid: ticker.bid,
                            ask: ticker.ask,
                            //baseVolume: ticker.baseVolume,
                            [baseFiat]: ticker.last/fiatRate(baseFiat, ticker.symbol.split('/')[1]),
                            [baseCryptocurrency]: -1,
                            fee_exchange: applicablefee
                        }

                        tickers.push(customTicker);
                        //console.log(exchange.name)
                        console.log(ticker.symbol +" "+ticker.last);
                        //console.log(ticker.last)
                    }
                ).catch((error)=>{
                    console.log(error)
                    return error})
            } 
        }
       
    }
    console.log("Finished loading tickers")
    return tickers
}

var tickers = []

var getBalances = async function(){
    var balances = []
    for(var i=0;i<exchanges.length;i++){
        await(exchanges[i].fetchBalance ()).then((response)=>{
            console.log(exchanges[i].name +" balance: ", response)
            let balance = {
                name: exchanges[i].name,
                free: response.free,
                used: response.used
            }
        balances.push(balance)
        }).catch((error)=>{
            console.log(error)
            return error})
       }
    return balances;
}

exports.getBalances = getBalances;

var init = async function() {
    console.log("START MERCURY")
    tickers = await fetchData(exchanges);
    setInterval(async function(){
        tickers = await fetchData(exchanges);
        console.log("LOADED "+tickers.length+" TICKERS!!");
    }, refreshRate*1000)
}


exports.getFees = function(){
  return fees
}


exports.getTickers = function(){
    return tickers
}

init()