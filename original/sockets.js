    //WORKS ONLY FOR BITSTAMP AND BTCMAKERTS FOR LTC
    //Initial rules: Trading afixed amount
    //1. Subscribe to the bid/ask order books of both exchanges. ex1 and ex2.
    //2. It currently only BUYS in Bitstamp and SElls in BTCMARKETS
    //3. Calculate the gap (bid-ask) of both exchanges. GAP1 and GAP2
    //4. Get LASTTRADING IN EX1 AND EX2
    //5a. Create variable MiddleGroundPrice = (LastPrice_EX1+LastPrice_EX2)/2
    //5b. Create variable push_percentage, that representes the initial % I want to create of offers with. Ex.: 6%
    //5c. Create varible pull_percentage, represents the min % the active order has to have, before I cancel it. Ex.: 4%
    //6a.  Send a limit BUY order in EX1 MiddleGroundPrice - push_percentage/2
    //6b.  Send a Limit SELL order in EX2 of MiddleGroundPrice + push_percentage/2
    //7.   Every time the socket sends me new price data I check if the current orders are still under the margin of "pull_percentage". 
    //     If they are not do nothing. If they are, remove old orders and create new ones with new values.

'use strict';
//LOAD MY DEPENDENCIES
const myexchanges = require('./myexchanges');
const myemail = require('./email');
 
//MY VARIABLES
let bestbid_Bitstamp_LTC;
let bestask_Bitstamp_LTC;
let bestbid_BTCMarkets_LTC; //in euros
let bestask_BTCMarkets_LTC; //in euros
let gap_Bistamp_LTC;
let gap_BTCMarkets_LTC;
let gainCurrent;
let gainMin;
let gainMax;
let lastPrice_BTCMarkets_LTC;
let lastPrice_BTCMarkets_LTC_EUR;
let lastPrice_Bitstamp_LTC;
let middleGroundPrice_LTC; // = (LastPrice_EX1+LastPrice_EX2)/2
let push_percentage = 1.018; //never put a value below 1. Note: Currentes fees are 0.65(BTC)+0.24(Bitstamp)+0.45(Transferwise)=1.34%, so I never put less than that
let pull_percentage = 1.014; //never put a value below 1. Ex. If push is 1.08 and pull 1.07, order will get cancelled and redone if they go up or down 1%
let pushBuyPrice;
let pushSellPrice;
let pullBuyPrice;
let pullSellPrice;
let lockedAmountToTrade = 0.07; //Value in double quotes to increase JSON compatibility (with simple can fail sometimes)
let decimals_LTC = 2;
let myopenbuyorder;
let myopensellorder;
let security_flag = 0; //Ensures that that function pulse doesnt execute if last execution didnt finish yet.
let security_vip_flag = false; //Flag that tells me that someone accepted my order and that I'm working on it. Don run other paralel processes while I'm dealing with it.
let operationID = ""; 
let flag_reachedmypullprice = false; //Flag that tells me I reached my pullprice and cancelled the original orders
let flag_canceledmybuyorder = false; //Flag that indicates I failed making my sell order and canceled my buy order, so I have to create new ones again
let flag_buyorderfailed = false; //Flag that indicates I failed making my initial buy, so I have to retry to create new ones again
let flag_processingBTCTradeinfo = false; //Flat that indicates I'm processing BTCMarkets trade info
var pulse_quoue = [];
let audtoeur = 0.63; //In the future it should download this number
let eurtoaud = 1/audtoeur;
const LTC_address_BTCMarkets = 'LLhAUNv1nCjU5SGrwbUNCtWDScSbpW92kB';

//SET UP DB CONNECTION
var mysql = require('mysql');
var con = mysql.createConnection({
  host: "localhost",
  user: "mercuryDB",
  password: "password",
  database: "mercury"
});
con.connect(function(err) {
   if (err) console.error(err);
   else console.log("Mysql Connected!");
}); 
//END DB SET UP
          

let historyBitstampOrdersSent = [];
let historyBTCMarketsOrdersSent = [];

//********** START OF BTCMARKETS ********* */
// SOCKET TO BTC Markets using sockert.io
var socket1 = require('socket.io-client')('https://socket.btcmarkets.net', {secure: true, transports: ['websocket'], upgrade: false});
var socket2 = require('socket.io-client')('https://socket.btcmarkets.net', {secure: true, transports: ['websocket'], upgrade: false});
var socket3 = require('socket.io-client')('https://socket.btcmarkets.net', {secure: true, transports: ['websocket'], upgrade: false});

socket1.on('connect', function(){
    try { socket1.emit('join', 'Ticker-BTCMarkets-LTC-AUD'); console.log('BTC Markets...tickers...connected'); } catch(error){console.log(error);}
    });
socket2.on('connect', function(){
    try { socket2.emit('join', 'Orderbook_LTCAUD'); console.log('BTC Markets...OrderBook...connected');} catch(error){console.log(error);}
      });
socket3.on('connect', function(){
     try { socket3.emit('join', 'TRADE_LTCAUD');  console.log('BTC Markets...MarketTrade...connected');} catch(error){console.log(error);}
      });

socket1.on('newTicker', function(data){
    /* Example of format received
                { volume24h: 59399231992,
            bestBid: 1086453000000,
            bestAsk: 1090000000000,
            lastPrice: 1094963000000,
            timestamp: 1518539116942,
            snapshotId: 1518539116942000,
            marketId: 2001,
            currency: 'AUD',
            instrument: 'BTC' }
    */
    //console.log("Received BTCMARKETS TICKERS")
    //console.log(data); 
    //Every time I receive new information on this socket I run my function to recalculate
    pulse_control("BTCMarkets_tickers", data);
    });

socket2.on('OrderbookChange', function(data) {
        console.log("Received BTCMARKETS ORDERBOOK DATA BEGIN...");
        console.log(data);
        console.log("Received BTCMARKETS ORDERBOOK DATA END");
    });

socket3.on('MarketTrade', async function(data) {
        //while (flag_processingBTCTradeinfo == true) {} //While I'm proceccsing a received Market Trade, if I receive another one, it should wait till I finished with the previous one.
        flag_processingBTCTradeinfo = true;
        //console.group("SOCKET BTCMARKETS MarketTrade");
        console.log("Received BTCMARKETS TRADE...let's check it out...");
        
        //console.log(data);
        /*console.log(data.trades)
        for (let i=0; i<data.trades.length; i++) {
            console.log("Number of trade received in package = "+(data.trades.length));
            console.log(i+" Price="+data.trades[i][1]);
            console.log(i+" Amount="+data.trades[i][2]);
            console.log(i+" ID="+data.trades[i][3]);  
        }
        */
            //Example of data received 

            /*
                 { id: 0,
                timestamp: 0,
                marketId: 2002,
                agency: 'BTCMarkets',
                instrument: 'LTC',
                currency: 'AUD',
                trades:
                [ [ 1518706947677, 28250000000, 100000000, 1276557544 ], 
                  [ 1518706947684, 28024000000, 600000000, 1276557555 ] ] }  
            */
                   //Only continue if historyBTCMarketsOrdersSent is not undefined (has values)
                
                    if(typeof historyBTCMarketsOrdersSent != "undefined" && historyBTCMarketsOrdersSent.length != 0) {
                             //Only continue if I find the live order in my array of orders (if it doesnt the search will return undefined)
                             //As I can have several trades in on received package I have to run a loop to for each trade operation I received
                             console.log("Number of BTCMarkets trades received in package = "+(data.trades.length));
                             //Structure of data.trades:
                             /*for (let i=0; i<data.trades.length; i++) {
                                console.log(i+" Price="+data.trades[i][1]);
                                console.log(i+" Amount="+data.trades[i][2]);
                                console.log(i+" BuyID="+data.trades[i][3]);
                                }*/
                        // IMPORTANT NOTE: BTCMarkets gives us the BUYOrderId of a trade but not the SELLORDERID.
                        // To fix this. Each time I see a trade I need to check the status of my SELLORDERID
                        // I need to convert both IDs to Strings, if i dont it doesn't find it
                        console.log("BTCMarkets traded. Lets check the status of my BTCMarkets order...");
                        let checkstatus1;
                        checkstatus1 = await fetchOrderBtcmarkets(myopensellorder.id); // historyBTCMarketsOrdersSent[0].id
                        console.log("BTCMarkets traded. Lets check the status of my BTCMarkets order..."+checkstatus1.status);
                            if(checkstatus1.status=="closed") { //If the order was closed (executed)
                                console.log("SELL EXECUTED. Order ID "+checkstatus1.id);
                                //Record in DB
                                var sqlQueryOrderDeletedBTCMarkets = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`,`ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`,  `ord_expected_gain`, `ord_exchangeorderid`, `ord_agent`) VALUES ('"+operationID+"','BTCMarkets', 'EXECUTED-COMPLETED', 'SELL',  'LTC', 'EUR', '"+pushSellPrice+"', '"+lockedAmountToTrade+"',  '"+pushSellPrice+"', '"+push_percentage+"','"+checkstatus1.id+"', 'ATM_Socket_Limit' );";
                                con.query(sqlQueryOrderDeletedBTCMarkets, function (err, result) {if (err) throw err; /*console.log("1 Order Mysql record inserted");*/ });
                                someoneAcceptedMyOrder('BTCMarkets_trade',checkstatus1);
                            } 
                            else {
                                console.log("BTCMarkets traded, but not our orders.")
                            }          
                    } 
    flag_processingBTCTradeinfo = false;
    //console.groupEnd();
    });

socket1.on('disconnect', function(){
	console.log('BTC Markets TICKERS disconnected');
    });
socket2.on('disconnect', function(){
	console.log('BTC Markets ORDERBOOK disconnected');
    });
socket3.on('disconnect', function(){
	console.log('BTC Markets TRADE disconnected');
    });
//********** END OF BTCMARKETS ********* */


//********** START OF BITSTAMP ********* */
// Bitstamp using pusher
const Pusher = require('pusher-js');
/*
var pusher = new Pusher({
  appId: 'APP_ID',
  key: 'APP_KEY',
  secret: 'SECRET_KEY',
  encrypted: ENCRYPTED, // optional, defaults to false
  host: 'HOST', // optional, defaults to api.pusherapp.com
  port: PORT, // optional, defaults to 80 for unencrypted and 443 for encrypted
  cluster: 'CLUSTER', // optional, if `host` is present, it will override the `cluster` option.
});*/


let pusher = new Pusher('de504dc5763aeef9ff52');
let tradesChannel = pusher.subscribe('live_trades_ltceur');
tradesChannel.bind('trade', function (data) {
                    /*Format of data returned here:
                    { amount: 0.00930237,
                        buy_order_id: 951804140,
                        sell_order_id: 951805372,
                        amount_str: '0.00930237',
                        price_str: '8518.06',
                        timestamp: '1518538731',
                        price: 8518.06,
                        type: 1,
                        id: 54496595 }
                    */ 
    //console.log("Bitstamp")
    //console.log(data)
    if(typeof myopenbuyorder != "undefined") { //If I have an openbuyorder check if its the one that just got traded.
        if(myopenbuyorder.id.toString() == data.buy_order_id.toString()) { 
                                //console.log("I just saw on live_trades_ltceur my buy order execute. I should have reacted to this! Amount="+data.amount+" Price="+data.price_str);
                                someoneAcceptedMyOrder("Bitstamp_trade_completed", data);
                                }
    }
    //Every time I receive new information on this socket I run my function to recalculate
    //PENDING WORK. Should I also call pulse_control if someone accepted my offer?
    pulse_control("Bitstamp_trade", data);

});

let orderBookChannel = pusher.subscribe('order_book_ltceur');
orderBookChannel.bind('data', function(data){
                                                                    /* Format of data returned here:
                                                                        { bids:
                                                                        [ [ '8518.07000000', '0.05238097' ],
                                                                            [ '8359.00000000', '0.03596262' ] ],
                                                                        asks:
                                                                        [ [ '8523.66000000', '3.97929193' ],
                                                                            [ '8525.00000000', '0.50559232' ],
                                                                            */
                                                //console.log("Bitstamp")
                                                //console.log(data)
                                                //Every time I receive new information on this socket I run my function to recalculate
                                                pulse_control("Bitstamp_orderbook", data);
                                            });


let liveOrdersChannel = pusher.subscribe('live_orders_ltceur');
liveOrdersChannel.bind('order_created', function(data){
                                                    //Only continue if historyBitstampOrdersSent is not undefined (has values)
                                                    if(typeof historyBitstampOrdersSent != "undefined" && historyBitstampOrdersSent.length != 0) {
                                                        //Only continue f I find the live order in my array of orders (if it doesnt the search will return undefined)
                                                        //I need to convert both IDs to Strings, if i dont it doesn't find it
                                                        if ((typeof historyBitstampOrdersSent.find(o => o.id.toString() === data.id.toString())) != "undefined") {
                                                            let o = historyBitstampOrdersSent.find(o => o.id.toString() === data.id.toString());
                                                            console.log("Bitstamp Order ID "+data.id.toString()+" CONFIRMED OPEN");
                                                            //Record IN DB
                                                            var sqlQueryOrdersBuyInsert = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`,`ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`,  `ord_expected_gain`, `ord_exchangeorderid`, `ord_agent`) VALUES ('"+operationID+"','Bitstamp', 'CONFIRMED', 'BUY',  'LTC', 'EUR', '"+pushBuyPrice+"', '"+lockedAmountToTrade+"',  '"+pushBuyPrice+"', '"+push_percentage+"','"+myopenbuyorder.id+"', 'ATM_Socket_Limit' );";
                                                            con.query(sqlQueryOrdersBuyInsert, function (err, result) {if (err) throw err; /*console.log("1 Order Mysql record inserted");*/ });
 
                                                        }
                                                        else {
                                                            //If I see an order that isn't mine do nothing
                                                        }
                                                      
                                                    }

                                                    });

liveOrdersChannel.bind('order_changed', function(data){
                                                    //Only continue if historyBitstampOrdersSent is not undefined (has values)
                                                    if(typeof historyBitstampOrdersSent != "undefined" && historyBitstampOrdersSent.length != 0) {
                                                        //Only continue f I find the live order in my array of orders (if it doesnt the search will return undefined)
                                                        //I need to convert both IDs to Strings, if i dont it doesn't find it
                                                        if ((typeof historyBitstampOrdersSent.find(o => o.id.toString() === data.id.toString())) != "undefined") {
                                                            let o = historyBitstampOrdersSent.find(o => o.id.toString() === data.id.toString());
                                                            console.log("Order ID "+data.id.toString()+" CHANGED. This implies that the order was executed PARTIALLY");
                                                            console.log(data);
                                                                    //Record  IN DB
                                                                    var sqlQueryOrdersBuyInsert = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`,`ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`,  `ord_expected_gain`, `ord_exchangeorderid`, `ord_agent`) VALUES ('"+operationID+"','Bitstamp', 'CHANGED', 'BUY',  'LTC', 'EUR', '"+pushBuyPrice+"', '"+lockedAmountToTrade+"',  '"+pushBuyPrice+"', '"+push_percentage+"','"+myopenbuyorder.id+"', 'ATM_Socket_Limit' );";
                                                                    con.query(sqlQueryOrdersBuyInsert, function (err, result) {if (err) throw err; /*console.log("1 Order Mysql record inserted");*/ });
                                                             //PENDING WORK! Manage what happens if I get a partial Buy/sell order
                                                        }
                                                        else {
                                                            //If I see an order that isn't mine do nothing
                                                        }
                                                      
                                                    }
                                            });
liveOrdersChannel.bind('order_deleted', function(data){ //BITSTAMP will DELETE the Order if I delete it or if it was executed. So I need to check which one of the two happened.
                                                        //When I create my myopenbuyorder I add a parameter called "mercurycanceledthisorder"to the object that indicates the "canceling status" of this order.
                                                        //3 values are accepted:
                                                        //mercurycanceledthisorder = no --> means I never tried to cancel the order
                                                    //1. Only continue if historyBitstampOrdersSent is not undefined (has values)
                                                    if(typeof historyBitstampOrdersSent != "undefined" && historyBitstampOrdersSent.length != 0) {
                                                        //2. Only continue f I find the live order in my array of orders (if it doesnt the search will return undefined)
                                                        //I need to convert both IDs to Strings, if i dont it doesn't find it
                                                        if ((typeof historyBitstampOrdersSent.find(o => o.id.toString() === data.id.toString())) != "undefined") {
                                                            let o = historyBitstampOrdersSent.find(o => o.id.toString() === data.id.toString());
                                                            console.log("Order ID "+data.id.toString()+" DELETED. This means it was either cancelled or executed. ");
                                                            //console.log(data);
                                                                                //data has the following format
                                                                            /* { price: 168.83,
                                                                                    amount: 0.07,
                                                                                    datetime: '1519379538',
                                                                                    id: 1016000346,
                                                                                    order_type: 0 }
                                                                                */
                                                            console.log("myopenbuyorder.mercurycanceledthisorder="+myopenbuyorder.mercurycanceledthisorder);
                                                            console.log("o.mercurycanceledthisorder="+o.mercurycanceledthisorder);
                                                            
                                                            //IF THE ORDER HAS A PENDING CANCEL ORDER I CAN NOW CONFIRM IT WAS EXECUTED
                                                            //PENDING!! NOT SURE WHICH OF THE 2 OBJECTS I NEED TO UPDATE. ONLY ONE IS ENOUGH (o.mercurycanceledthisorder or myopenbuyorder.mercurycanceledthisorder)
                                                            if(myopenbuyorder.mercurycanceledthisorder =="waiting bitstamp to execute cancel order") {
                                                                 myopenbuyorder.mercurycanceledthisorder = "cancel executed"; 
                                                                 console.log("Bitstamp executed my cancel order.");
                                                                 //Record IN DB
                                                                var sqlQueryOrderDeleted = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`,`ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`,  `ord_expected_gain`, `ord_exchangeorderid`, `ord_agent`) VALUES ('"+operationID+"','Bitstamp', 'DELETED-Executed', 'BUY',  'LTC', 'EUR', '"+pushBuyPrice+"', '"+lockedAmountToTrade+"',  '"+pushBuyPrice+"', '"+push_percentage+"','"+myopenbuyorder.id+"', 'ATM_Socket_Limit' );";
                                                                con.query(sqlQueryOrderDeleted, function (err, result) {if (err) throw err; /*console.log("1 Order Mysql record inserted");*/ });
 
                                                            } 
                                                            else {   
                                                                    console.log("Bistamp deleted an order that I did not try to cancel")   
                                                                    //Record IN DB
                                                                    var sqlQueryOrderDeleted = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`,`ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`,  `ord_expected_gain`, `ord_exchangeorderid`, `ord_agent`) VALUES ('"+operationID+"','Bitstamp', 'DELETED-Cancelled', 'BUY',  'LTC', 'EUR', '"+pushBuyPrice+"', '"+lockedAmountToTrade+"',  '"+pushBuyPrice+"', '"+push_percentage+"','"+myopenbuyorder.id+"', 'ATM_Socket_Limit' );";
                                                                    con.query(sqlQueryOrderDeleted, function (err, result) {if (err) throw err; /*console.log("1 Order Mysql record inserted");*/ });
                                                                 }
                                                        }
                                                        else {
                                                            //If I see an order that isn't mine do nothing
                                                        }
                                                    }
                                            });   
//********** END OF BITSTAMP  ********* */


//Returns Current Timestamp if format: 2018-02-28 h:m:s
function getFormattedDate() {
    var date = new Date();
    var str = date.getFullYear() + "-" + ( "0"+(date.getMonth() + 1)).slice(-2) + "-" +("0" + date.getDate()).slice(-2) + "-" +  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    return str;
}

/**
 * @param num The number to round
 * @param precision The number of decimal places to preserve
 */
function roundUp(num, precision) {
    precision = Math.pow(10, precision)
    return Math.ceil(num * precision) / precision
  }

async function someoneAcceptedMyOrder(exchange, data) {
    security_vip_flag = true;  //Tells my pulse function to not do anything more and wait for me to finish
    //If someone accepted my Bistamp order, cancel the order of BTCMarkets and execute at market price
    //The same the other way around
    while(security_flag) {} //While security_flag is active, wait. Once it finishes I can run this. If not the parallel processing will screw everything up.
    switch(exchange) {
        case "BTCMarkets_trade":
                                console.log("EXECUTED SELL ORDER in BtcMarkets. Status="+data.status+" Filled="+data.filled+" Price="+data.price+" Amount="+data.amount+" ID="+data.id+" Remaining="+data.remaining);
                                //RECORD ID DB THAT I EXECUTED ORDER IN BTCMARKETS 
                                var sqlQueryLimitSellOrderExecuted = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`,`ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`,  `ord_expected_gain`, `ord_exchangeorderid`, `ord_agent`) VALUES ('"+operationID+"','BTCMarkets', 'EXECUTED-SELL', 'LIMIT-SELL',  'LTC', 'EUR', '"+data.price+"', '"+data.amount+"',  '', '','"+data.id+"', 'ATM_Socket_Limit' );";
                                con.query(sqlQueryLimitSellOrderExecuted, function (err, result) {if (err) throw err;/* console.log("1 Order Mysql record inserted");*/ });                                                                      
                                
                                //I need to cancel my Bitstamp order and make a new one at market price
                                let cancelBitstampOrderReply;
                                cancelBitstampOrderReply = await cancelBitstampOrder(); 

                                 if(cancelBitstampOrderReply=="ok") { console.log("The cancelation of Bitstamp limit order has been succesfully sent and confirmed executed by Bitstamp.");}
                                 else {console.log("My limit order was NOT cancelled correctly. For the moment I don't care (you should cancel it manualy). Executing a new one anyway... ");}
                                                                       
                                 //Send Market Order on Bitstamp
                                await createMarketOrderBitstamp(data.amount);
                                //If Market order was succesful I finished the cycle
                                await successful_cycle();
                                break;

        case "Bitstamp_trade_completed":
                                //RECORD IN DB THAT I EXECUTED ORDER IN BITSTAMP
                                var sqlQueryLimitBuyOrderExecuted = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`,`ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`,  `ord_expected_gain`, `ord_exchangeorderid`, `ord_agent`) VALUES ('"+operationID+"','Bitstamp', 'EXECUTED-BUY', 'LIMIT-BUY',  'LTC', 'EUR', '"+data.price+"', '"+data.amount+"',  '', '','"+data.buy_order_id+"', 'ATM_Socket_Limit' );";
                                con.query(sqlQueryLimitBuyOrderExecuted, function (err, result) {if (err) throw err;/* console.log("1 Order Mysql record inserted");*/ });                                      
                                                            
                                console.log("EXECUTED BUY ORDER in Bitstamp at Price="+data.price.toString()+" Amount="+data.amount.toString()+" ID="+data.buy_order_id.toString()+". I going to cancel BTCMarkets limit counter order and make a new limit order...");
                                
                                //I need to cancel my BTCMarkets order (if it exists) and make a new one at market price
                                if(typeof myopensellorder == "undefined") { //If BTCMarkets order was never executed no need to cancel
                                     //Send Market Order on BTCMarkets
                                     console.log("I dont seem to have any open sell order in BTCMarkets. No need to cancel anything")
                                     await createMarketOrderBTCMarkets(data.amount.toString());
                                }
                                else {


                                    let cancelBTCMarketsOrderReply;
                                    cancelBTCMarketsOrderReply = await cancelBTCMarketsOrder();

                                    //Confirm I cancelled correctly
                                    if(cancelBTCMarketsOrderReply=="ok") {
                                        console.log("BTCMarket limit order has been succesfully canceled.");
                                        //Send Market Order on BTCMarkets
                                        await createMarketOrderBTCMarkets(data.amount.toString());
                                    } 
                                    else if(cancelBTCMarketsOrderReply=="not ok - closed instead") { //If order wasnt cancelled because it was closed then its perfect. No idea to create a new market order
                                        console.log("My limit order was NOT cancelled, because it was CLOSED. Operation successfully completed.");
                                    } 
                                    else {
                                        console.log("My limit order was NOT cancelled correctly. For the moment I dont care (you should cancel it manualy). Executing a new one... ");
                                        //Send Market Order on BTCMarkets
                                        await createMarketOrderBTCMarkets(data.amount.toString());
                                    }
                                }
            
                                /*
                                var mybtbuyorder;
                                try {   console.log("I'm going to try sell at maket price at BTCMARKETS...")
                                        mybtbuyorder = await myexchanges.btcmarkets.createOrder('LTC/AUD','market','sell',data.amount.toString()); //Ex: createLimiBuyOrder(BCH/EUR,market,buy,1.5)
                                        console.log(mybtbuyorder);
                                        //Record IN DB
                                        var sqlQueryOrdersBuyInsertInstant = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`,`ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`,  `ord_expected_gain`, `ord_exchangeorderid`, `ord_agent`) VALUES ('"+operationID+"','BTCMarkets', 'BUYMARKET', 'COUNTER-SELL',  'LTC', 'EUR', 'market', '"+data.amount+"',  '', '','"+mybtmarketbuyorder.id+"', 'ATM_Socket_Limit' );";
                                        con.query(sqlQueryOrdersBuyInsertInstant, function (err, result) {if (err) throw err; });                                                                      
                                } catch(error) { console.log("Something went wrong. I could'nt complete the MAKET BUY order!!"+error);  }
                                */
                                //PENDING TO CONFIRM AT WHAT FINAL PRICE I TRADED, CALCULATE REAL GAIN, LOG IT IN DB AND SEND EMAIL WITH REPORT
                                break;
        default:
    }
    security_vip_flag = false;
}

/**
 * Report successful cycle, calculate final gains, log in DB, send email and reactivate pulse to start all over again
 */
async function successful_cycle()
{ 
    //PENDING calculate final gains, log in DB, and reactivate pulse to start all over again.
    console.log("CONGRATULATIONS! Buy and Sell orders completed succesfully. We made MONEY. You're welcome.");
    historyBTCMarketsOrdersSent.shift(); //Remove my BTCMarkets order from the array so it doesn't get procecessed again.

    console.log("Transfering LTC Funds from Bitstamp to BTCMarkets...");

    try {
        let withdrawresult = await myexchanges.bitstamp.withdraw('LTC', lockedAmountToTrade.toString(), LTC_address_BTCMarkets);
        console.log("Successful LTC Withdrawal");
        console.log(withdrawresult);
    }
    catch(error) {
        console.log("Error Transfering LTCs from BITSTAMP TO BTCMarkets")
        console.log(error);
    }
         //Send us an email so we operate manually
         var mailOptions = {
                            from: 'mercury@criadoperez.com',
                            to: 'alejandro@criadoperez.com, christian@criadoperez.com, pablo@criadoperez.com',
                            subject: 'Mercury Sockets made money',
                            text: 'CONGRATULATIONS! Buy and Sell orders completed. We made MONEY. Youre welcome. And dont worry I also transferred the coins from one exchange to the other.'
                            }; 
        myemail.myemail.sendMail(mailOptions, function(error, info){
            if (error) {console.log("Error trying to send email:"+error);} else {console.log('Email sent: ' + info.response);}
                }); 
          //End of Send email
    

}
/** 
 * Creates a Market order in Bitstamp and logs it in the Database
*/
async function createMarketOrderBitstamp(amounttobuy) {                                          
                                                    //Sending Market Orders fails very often. So if it fails, retry a few times
                                                    var tryafewtimes = timeoutms => new Promise((r, j)=>{
                                                        var check = () => {
                                                          console.log("Let try to execute a MarketOrder in Bitstamp...");
                                                          var mybsmarketbuyorder;
                                                          try { mybsmarketbuyorder = myexchanges.bitstamp.createOrder('LTC/EUR','market','buy',amounttobuy); //Ex: createLimiBuyOrder(BCH/EUR,market,buy,1.5)
                                                                  //Record IN DB
                                                                  var sqlQueryOrdersBuyInsertInstant = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`,`ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`,  `ord_expected_gain`, `ord_exchangeorderid`, `ord_agent`) VALUES ('"+operationID+"','Bitstamp', 'BUYMARKET', 'COUNTER-BUY',  'LTC', 'EUR', 'market', '"+amounttobuy+"',  '', '','"+mybsmarketbuyorder.id+"', 'ATM_Socket_Limit' );";
                                                                  con.query(sqlQueryOrdersBuyInsertInstant, function (err, result) {if (err) throw err; /*console.log("1 Order Mysql record inserted");*/ });
                                                                  //If this worked respond the Promise
                                                                  r('ok');
                                                          } catch(error) {
                                                                         console.log("Something went wrong. I could'nt complete the MARKET BUY order. I'll try again... Error="+error);  
                                                                         if((timeoutms -= 3000) < 0) j('Retried maximum number of times!');
                                                                         else setTimeout(check, 3000);
                                                                        }
                                                        }
                                                        setTimeout(check, 3000);
                                                      })
                                                   
                                                    let resultofmycobpromise = await tryafewtimes(21000); 
                                                    console.log("I have a result of my promise to create Market Order in Bitstamp, it is="+resultofmycobpromise);
                                                    

/*
    var mybsmarketbuyorder;
    try { mybsmarketbuyorder = await myexchanges.bitstamp.createOrder('LTC/EUR','market','buy',amounttobuy); //Ex: createLimiBuyOrder(BCH/EUR,market,buy,1.5)
            //Record IN DB
            var sqlQueryOrdersBuyInsertInstant = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`,`ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`,  `ord_expected_gain`, `ord_exchangeorderid`, `ord_agent`) VALUES ('"+operationID+"','Bitstamp', 'BUYMARKET', 'COUNTER-BUY',  'LTC', 'EUR', 'market', '"+amounttobuy+"',  '', '','"+mybsmarketbuyorder.id+"', 'ATM_Socket_Limit' );";
            con.query(sqlQueryOrdersBuyInsertInstant, function (err, result) {if (err) throw err;  });                                      
    } catch(error) { console.log("Something went wrong. I could'nt complete the MARKET BUY order!!"+error);  }
*/
   
}

/** 
 * Creates a Market order in BTC Markets and logs it in the Database
*/
async function createMarketOrderBTCMarkets(amounttosell) {
    var mybtbuyorder;
    console.log("I'm going to try sell at market price at BTCMARKETS...");
    try {  
            mybtbuyorder = await myexchanges.btcmarkets.createOrder('LTC/AUD','market','sell',amounttosell); //Ex: createLimiBuyOrder(BCH/EUR,market,buy,1.5)
            console.log(mybtbuyorder);
            console.log("I'm going to try sell at market price at BTCMARKETS...COMPLETED")
            //Record IN DB
            var sqlQueryOrdersBuyInsertInstant = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`,`ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`,  `ord_expected_gain`, `ord_exchangeorderid`, `ord_agent`) VALUES ('"+operationID+"','BTCMarkets', 'BUYMARKET', 'COUNTER-SELL',  'LTC', 'EUR', 'market', '"+amounttosell+"',  '', '','"+mybtmarketbuyorder.id+"', 'ATM_Socket_Limit' );";
            con.query(sqlQueryOrdersBuyInsertInstant, function (err, result) {if (err) throw err;/* console.log("1 Order Mysql record inserted");*/ });                                                                      
    } catch(error) {  //It it fails, try once more (It usually works on the second try sometimes)
                    try {
                        console.log("Something went wrong. I could'nt complete the BTCMARKETS MARKET SELL order. Let me try again. Error was="+error);  
                        mybtbuyorder = await myexchanges.btcmarkets.createOrder('LTC/AUD','market','sell',amounttosell); //Ex: createLimiBuyOrder(BCH/EUR,market,buy,1.5)
                        console.log(mybtbuyorder);
                        console.log("I'm going to try sell at market price at BTCMARKETS...COMPLETED")
                         //Record IN DB
                        var sqlQueryOrdersBuyInsertInstant = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`,`ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`,  `ord_expected_gain`, `ord_exchangeorderid`, `ord_agent`) VALUES ('"+operationID+"','BTCMarkets', 'BUYMARKET', 'COUNTER-SELL',  'LTC', 'EUR', 'market', '"+amounttosell+"',  '', '','"+mybtmarketbuyorder.id+"', 'ATM_Socket_Limit' );";
                        con.query(sqlQueryOrdersBuyInsertInstant, function (err, result) {if (err) throw err;/* console.log("1 Order Mysql record inserted");*/ });    
                    } catch(error) {
                                     console.log("Something went wrong. I could'nt complete the BTCMARKETS MARKET SELL order. I tried twice!!"+error);
                                     //PENDING I SHOULD TRY TO EXECUTE A LIMIT ORDER INSTEAD IF THE MARKET ORDER FAILED TWICE
                    }
    }
}

/**
 * This function returns in an array all indexes in the array that contain a certain value
 * Only valid for the array pulse_quoue with parameter "eventname"
 * @param {*} arr 
 * @param {*} val 
 */
function getAllIndexes(arr, val) {
    var indexes = [], i;
    for(i = 0; i < arr.length; i++) {
        if (typeof arr[i] != "undefined") {//Ignore undefined elements of the array
                                            if (arr[i].eventname === val)
                                            indexes.push(i);
        }
    }
    return indexes;
}

/*
*   PULSE_CONTROL, receives pulse events from several sources.
*   This function makes sure it only runs one event at at time. If the pulse function is busy it adds the event to a quoue, and next time it can, it executes all pending events in the quoue
*/
async function pulse_control(exchange,data) { //Cola del pulse.
    pulse_quoue.push({eventname: exchange, eventdata: data}); //Add the object to the end of the array

  
    if(security_flag == 1 || security_vip_flag || flag_processingBTCTradeinfo) { //Don't execute the pulse  function if my flags are on. 
        console.log("Update from "+exchange+" added to the quoue. I'm busy. Quoue has "+pulse_quoue.length+" events. Security_flag="+security_flag+" security_vip_flag="+security_vip_flag+" flag_processingBTCTradeinfo="+flag_processingBTCTradeinfo);  
    }
    else { 
        while(pulse_quoue.length>0 && security_vip_flag==false && flag_processingBTCTradeinfo==false) { //As long as I have elements in the quoue process them in order. Except if I have a VIP process running or wanting to run
            //console.log("Quoue is "+pulse_quoue.length+". Processing an event from "+pulse_quoue[0].eventname);
            
            //S1 BEGIN -- If I have several BITSTAMP TICKERS in the quoue, I should only use the last one. The rest are obsolete.
            let indexeswithbitstamporderbooks = [];
            indexeswithbitstamporderbooks = getAllIndexes(pulse_quoue, "Bitstamp_orderbook");
            if(indexeswithbitstamporderbooks.length>1) {console.log("Bitstamp_orderbook appears "+indexeswithbitstamporderbooks.length+" times in the quoue at positions="+indexeswithbitstamporderbooks);}
            for(let l=0; l<(indexeswithbitstamporderbooks.length -1 ); l++) { //If my array only has 1 value it won't enter the loop
                //If my array has more than one value I want to eliminate from the quoue the obsolete event
                delete pulse_quoue[indexeswithbitstamporderbooks[l]] //delete changes the element to undefined, does'nt change other indexes
                console.log("Eliminated a obsolete Bitstamp_orderbook event in the quoue at position="+indexeswithbitstamporderbooks[l])
            }
    
            while(typeof pulse_quoue[0] == "undefined") { //While to next element to process is undefined it means I deleted it and should not be processed
                pulse_quoue.shift();
                console.log("Ignoring deleted event in the quoue");
                     }
            //S1 END

            await pulse(pulse_quoue[0].eventname, pulse_quoue[0].eventdata);
            pulse_quoue.shift(); //Remove first event in the quoue that I just processed.
        }
    }
    /*
    switch(security_flag) {
        case 1:
                //Don't execute the function if the security flag is on. 
                console.log("Added an update from "+exchange+" to the quoue. I'm too busy to take care of that now. Quoue has "+pulse_quoue.length+" events in line.");  
                 break;
        case 0: while(security_vip_flag) { } //This will wait in an eternal loop my security_vip_flag or flag_processingBTCTradeinfo, are on indicating I should wait.
                //If pulse is not execution, execute for all elements in the quoue in the received order.
                 //If the array is modified during iteration "forEach", other elements might be skipped. To avoid this I check the lenght first
                while(pulse_quoue.length>0 && security_vip_flag==false) { //As long as I have elements in the quoue process them in order. Except if I have a VIP process running or wanting to run
                    //console.log("Quoue is "+pulse_quoue.length+". Processing an event from "+pulse_quoue[0].eventname);
                    await pulse(pulse_quoue[0].eventname, pulse_quoue[0].eventdata);
                    pulse_quoue.shift(); //Remove first event in the quoue that I just processed.
                }
                break;
        default:

    }*/
}

//Every time I receive new data this function updates my variables. It my pulse.
async function pulse(exchange, data) {
    if(security_flag==1) { //Don't execute the function if the security flag is on. 
                            console.log("Ignoring an update from "+exchange+". I'm busy. This mesage SHOULD NEVER APPEAR unless there is a bug."); return; 
                        }
    security_flag = 1; //Set my flag to 1, informing I'm starting the process
    switch(exchange) { //Check which is the Exchange I received info on, and update my variables accordignly
            case "Bitstamp_orderbook":
                            //console.log("Received info from "+exchange);
                            bestbid_Bitstamp_LTC = data.bids[0][0];
                            bestask_Bitstamp_LTC = data.asks[0][0];
                            gap_Bistamp_LTC = bestask_Bitstamp_LTC / bestbid_Bitstamp_LTC; //Gap in percentage
                            //console.log(data);
                            //console.log("Best bid: "+data.bids[0][0]);
                            //console.log("Best ask: "+data.asks[0][0]);
                            break;
            case "Bitstamp_trade":
                            lastPrice_Bitstamp_LTC = data.price;
                            //data.buy_order_id
                            break;
            case "BTCMarkets_tickers":
                            //console.log("Received info from "+exchange);
                            bestbid_BTCMarkets_LTC = (data.bestBid/100000000)*audtoeur;
                            bestask_BTCMarkets_LTC = (data.bestAsk/100000000)*audtoeur;
                            gap_BTCMarkets_LTC = bestask_BTCMarkets_LTC / bestbid_BTCMarkets_LTC; //Gap in percentage
                            lastPrice_BTCMarkets_LTC = data.lastPrice;
                            lastPrice_BTCMarkets_LTC_EUR = (data.lastPrice/100000000) * audtoeur;
                            //console.log("Best bid: "+data.bestBid/100000000);
                            //console.log("Best ask: "+data.bestAsk/100000000);
                            break;
            case "BTCMarkets_trade":
                            //PENDING WORK. 
                            //Should I update lastprices after my order has been completed? I dont because I dont think I should but not sure
                            break;
            default:
            }
        
        //If I still havent received the information for all of my exchanges dont do anything
        if(typeof bestbid_Bitstamp_LTC == "undefined" || typeof bestbid_BTCMarkets_LTC == "undefined" || typeof lastPrice_Bitstamp_LTC == "undefined" || typeof bestask_Bitstamp_LTC == "undefined") {    
            console.log("Received some data from "+exchange+", waiting for some more to start working...");
            security_flag = 0; //Set my flag to value that I finished
            return;
        }

        //Recalculate my gains
        gainCurrent = bestbid_BTCMarkets_LTC / bestask_Bitstamp_LTC;
        gainMax = push_percentage;
        gainMin = Math.min(bestbid_BTCMarkets_LTC / pushBuyPrice, pushSellPrice / bestask_Bitstamp_LTC);
        
        
        //MigddleGroundPrice_LTC can be calculated from last trade or from bid/asks
        //middleGroundPrice_LTC = (bestask_Bitstamp_LTC + bestbid_BTCMarkets_LTC) / 2;
        middleGroundPrice_LTC = (lastPrice_Bitstamp_LTC + lastPrice_BTCMarkets_LTC_EUR) /2;

        //Recalculate my pull prices
        pullSellPrice = middleGroundPrice_LTC * (((pull_percentage -1 )/ 2) +1 ) * eurtoaud;
        pullBuyPrice = middleGroundPrice_LTC * (((pull_percentage -1 )/ 2) -1 ) * (-1);

        //Check I need to update a push price or not. I update if it still doesnt exist or if it reached my pullprice
        //If its the first time I run (pushBuyPrice or pushSellPrice are undefined), or if some flags are active it means I have to create a new set of orders
        if(typeof pushBuyPrice == "undefinded" || typeof pushSellPrice =="undefined" || flag_reachedmypullprice == true || flag_canceledmybuyorder == true || flag_buyorderfailed == true) {
            await createNewSetOfOrders();
        } 
        else if (pushBuyPrice > pullBuyPrice || pushSellPrice < pullSellPrice ){ //Checks if my open buy/sell are still acceptable.
             //Price has changed, so I need to cancel my orders and create new ones
            console.log("Pull prices reached. I Cancel my current orders and make new ones. pushBuyPrice:"+pushBuyPrice+" pullBuyPrice:"+pullBuyPrice+" pushSellPrice:"+pushSellPrice+" pullSellPrice:"+pullSellPrice);
            console.log("flag_processingBTCTradeinfo="+flag_processingBTCTradeinfo+" flag_reachedmypullprice="+flag_reachedmypullprice+ " flag_canceledmybuyorder="+flag_canceledmybuyorder+" flag_buyorderfailed="+flag_buyorderfailed);
            await cancelSetOfOrders();
        } //CLOSING PULLPRICES CHECK IF
        //My current gain is what I would make now with current bids/asks. 
        //My minimum gain I how much I should make in worst case scenario that I operate (PushBuy/Ask or PushSell/Bid)
        //My maximum gain I how much I should make in best case scenario that I operate (both push orders are executed)
        console.log("Gain(current/min/max)="+gainCurrent.toString().slice(0,5)+"/"+gainMin.toString().slice(0,5)+"/"+gainMax.toString().slice(0,5)+" Gap(E1/E2)="+gap_Bistamp_LTC.toString().slice(0,5)+"/"+gap_BTCMarkets_LTC.toString().slice(0,5)+" MG="+middleGroundPrice_LTC.toString().slice(0,6)+" BuyPrice(Push/Pull)="+pushBuyPrice.toString().slice(0,7)+"/"+pullBuyPrice.toString().slice(0,7)+" SellPrice(Pull/Push)="+pullSellPrice.toString().slice(0,7)+"/"+pushSellPrice.toString().slice(0,7)+" Q="+pulse_quoue.length+" R="+exchange);
        security_flag = 0; //Set my flag to 0 informing I finished.
}

/*
* CREATES NEW PAIR OF OR LIMIT ORDERS
*/
async function createNewSetOfOrders() {
    console.group("createNewSetOfOrders"); //Group the logs of this functions
    if(flag_reachedmypullprice) {flag_reachedmypullprice = false;} //If I'm creating new orders because I reached my pullprice, I reset my flag
    if(flag_canceledmybuyorder) {flag_canceledmybuyorder = false; } //If I'm creating new orders because I canceled my buy order, I reset my flag
    if(flag_buyorderfailed) {flag_buyorderfailed = false; } //If I'm creating new orders because my buy order failed, I reset my flag
    //Generate my new Operation ID
    operationID = getFormattedDate()+"-"+Math.random().toString(36).substr(2, 9);
    console.log("Generated a new Operation ID:"+operationID);

    //I create my first orders. toFixed converts number to string forcing 2 digits. Ej:272.3 transforms to 272.30. This is required for create Orders
    pushSellPrice = roundUp(middleGroundPrice_LTC * (((push_percentage -1 )/ 2) +1 ) * eurtoaud, decimals_LTC);
    pushBuyPrice = roundUp(middleGroundPrice_LTC * (((push_percentage -1 )/ 2) -1 ) * (-1), decimals_LTC);
    console.log("Lets send my first new buy order at "+pushBuyPrice+" and sell order at "+pushSellPrice);
    //console.log(myexchanges.bitstamp.name);
    try {
        console.log("BUY Order to Bitstamp "+lockedAmountToTrade+"LTC@"+pushBuyPrice+"€ BestAsk="+bestask_Bitstamp_LTC+"....TRYING TO SEND...");
        myopenbuyorder = await myexchanges.bitstamp.createOrder('LTC/EUR','limit','buy',lockedAmountToTrade,pushBuyPrice);
        myopenbuyorder.mercurycanceledthisorder = "no"; //Adds an element to my object buy order, that will later store the status of its cancelation.
        historyBitstampOrdersSent.push(myopenbuyorder); //Copy the the order to my history Array of orders in Bitstamp
        console.log("BUY Order to Bitstamp "+lockedAmountToTrade+"LTC@"+pushBuyPrice+"€. BestAsk="+bestask_Bitstamp_LTC+" Order ID="+myopenbuyorder.id+"...SENT.");
                    //Record SUCCESFULL OPEN ORDER WITH ORDERID IN DB
                    var sqlQueryOrdersBuyInsert = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`, `ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`,  `ord_expected_gain`, `ord_exchangeorderid`, `ord_agent`) VALUES ('"+operationID+"',  'Bitstamp', 'SENT', 'BUY',  'LTC', 'EUR', '"+pushBuyPrice+"', '"+lockedAmountToTrade+"',  '"+pushBuyPrice+"', '"+push_percentage+"','"+myopenbuyorder.id+"', 'ATM_Socket_Limit' );";
                    con.query(sqlQueryOrdersBuyInsert, function (err, result) {if (err) throw err; /*console.log("1 Order Mysql record inserted");*/ });

    } catch(error) {
                    console.error("Something went wrong. I couldnt complete the BUY ORDER..."+error); 
                    flag_buyorderfailed = true;
                }
    if(flag_buyorderfailed == false) { //Only send order to BTCMARKETS IF BITSTAMP WAS SUCCESFUL
        //BTCMARKERS orders fails often. So if it doesnt work, I retry to send the order until a maximum limit
        //let BTCtries_max = 5;
        //let BTCtries_counter = 0;
        //while(BTCtries_counter<BTCtries_max) {
        console.log("SELL Order to BTCMARKETS "+lockedAmountToTrade+"LTC@"+pushSellPrice+"$AUD BestBid="+bestbid_BTCMarkets_LTC/audtoeur+"$AUD ....TRYING TO SEND...");
        try {
                myopensellorder = await myexchanges.btcmarkets.createOrder('LTC/AUD','limit','sell',lockedAmountToTrade,pushSellPrice.toFixed(decimals_LTC));
                historyBTCMarketsOrdersSent.push(myopensellorder);  //Copy the ID of the order to my history Array of orders in BTCMarkets
                //BTCtries_counter = BTCtries_max; //If the order was sent correcty set my counter so it doesnt retry again.
                console.log("SELL Order to BTCMARKETS "+lockedAmountToTrade+"LTC@"+pushSellPrice+"$AUD. BestBid="+bestbid_BTCMarkets_LTC/audtoeur+"$AUD Order id="+myopensellorder.id+"...SENT.");
                         //Record SUCCESFULL OPEN ORDER WITH ORDERID IN DB
                         var sqlQueryOrdersSellInsert = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`,`ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`,  `ord_expected_gain`, `ord_exchangeorderid`, `ord_agent`) VALUES ('"+operationID+"', 'BTCMarkets', 'SENT', 'SELL',  'LTC', 'AUD', '"+pushSellPrice+"', '"+lockedAmountToTrade+"',  '"+pushSellPrice+"', '"+push_percentage+"','"+myopensellorder.id+"', 'ATM_Socket_Limit' );";
                         con.query(sqlQueryOrdersSellInsert, function (err, result) {if (err) throw err; /*console.log("1 Order Mysql record inserted");*/ });        
            } 
                catch(error) {
                            if(error.message=="Invalid amount.") { 
                            //if(error.message=="Invalid amount." && BTCtries_counter<BTCtries_max) { 
                                        //If it failed because of Invalid amount retry until maximum amount of retries is reaches                                                        
                                        //BTCtries_counter++;
                                        //console.log("Retrying..."+BTCtries_counter);
                                        console.log("Its says invalid amount..."+error)
                                                                                            } 
                            else {
                                    //BTCtries_counter = BTCtries_max; //This shouldnt be necesary but its a precaution to avoid infinite loops
                                    //console.error("Something went wrong. I could'nt complete the SELL ORDER....Number of tries:"+BTCtries_counter+" Error:"+error); 
                                    console.error("Something went wrong. I could'nt complete the SELL ORDER....Error:"+error); 
                                    console.log("We need to cancel the BUY order I made");

                                    let mycancelreply1 = await cancelBitstampOrder();
                                    if(mycancelreply1 == "ok") {flag_canceledmybuyorder = true;}
                                    else { //Send email asking for help if the program failed
                                              //Send us an email so we operate manually
                                                var mailOptions = {
                                                                 from: 'mercury@criadoperez.com',
                                                                to: 'alejandro@criadoperez.com, christian@criadoperez.com, pablo@criadoperez.com',
                                                                subject: 'Mercury Sockets Needs help - BUY order with no SELL order',
                                                                text: 'I sent a BUY order to Bitstamp ok. After that I tried to send a SELL order to BTCMarkets but it failed. So later, I tried to cancel the BUY order in Bitstamp, and I also failed to cancel it. Please do it manually.:'+error
                                                                }; 
                                                myemail.myemail.sendMail(mailOptions, function(error, info){
                                                                                                if (error) {console.log("Error trying to send email:"+error);} else {console.log('Email sent: ' + info.response);}
                                                                                            }); 
                                                 //End of Send email
                                    }
                           } //Closing else
                    } //Closing catch

                //}//Closing while
            }//Closing if
    //
    //myopensellorder = await myexchanges.btcmarkets.createLimitSellOrder('LTC/AUD',lockedAmountToTrade, pushSellPrice);
    //console.log(JSON.stringify(bitstamp.fetchOrder(myopenbuyorder.info),null,4));
    //console.log(JSON.stringify(bitstamp.fetchOrder(myopenbuyorder.id),null,4));
    //console.log(JSON.stringify(myexchanges.btcmarkets.fetchOrder(myopensellorder.id),null,4));
    //console.log(myexchanges.bitstamp.createLimit("ltceur"));
    console.groupEnd();
}

/**
 * Cancel BITSTAMP ORDER
 */
async function cancelBitstampOrder() {
    let cancelorderReply1;
    myopenbuyorder.mercurycanceledthisorder = "waiting bitstamp to execute cancel order";
    try{
        console.log("Cancelling Bitstamp order "+myopenbuyorder.id+" with mercurycanceledthisorder="+myopenbuyorder.mercurycanceledthisorder+"...");
        cancelorderReply1 = await myexchanges.bitstamp.cancelOrder(myopenbuyorder.id); //bitstamp returns this with cancelOrder: { price: 182.54, amount: 0.11, type: 0, id: 979854298 }            
        console.log("Cancelling Bitstamp order "+myopenbuyorder.id+" with mercurycanceledthisorder="+myopenbuyorder.mercurycanceledthisorder+" Reply:");
        console.log(cancelorderReply1);
    } catch(error) {
                    myopenbuyorder.mercurycanceledthisorder = "failed";
                    console.log("Couldn't cancel my bitstamp order. Error:"+error);
                }

     //Check the status of my orders
      //Bitstamp fetchOrder fails very often so I try a different strategy below
      //This "ok" just means bitstamp received and that its processing. I need to wait for socket confirmation to see when it is finally cancelled
    if(cancelorderReply1.id == myopenbuyorder.id ) {
                                                    //myopenbuyorder.mercurycanceledthisorder = "waiting bitstamp to execute cancel order";
                                                    //Wait to received socket confirmation. If none is received after timeout something went wrong
                                                    
                                                    //console.log("Lets make my promise...");
                                                    var waitForBitstampSocketConfirmation = timeoutms => new Promise((r, j)=>{
                                                        var check = () => {
                                                          console.log("Checking if Bitstamp executed my cancel order...");
                                                          if(myopenbuyorder.mercurycanceledthisorder == "cancel executed")   r('ok');
                                                          else if((timeoutms -= 100) < 0)                                    j('timed out!');
                                                          else                                                               setTimeout(check, 100);
                                                        }
                                                        setTimeout(check, 100);
                                                      })
                                                    
                                                      //console.log("DEBUG 111--")
                                                   
                                                    let resultofmypromise = await waitForBitstampSocketConfirmation(4000); 
                                                    console.log("I have a result of my promise, it is="+resultofmypromise);
                                                    return resultofmypromise;//Return the result of my promise
                                                            
    }
    else {
        myopenbuyorder.mercurycanceledthisorder = "Something is not ok";
        return "not ok";
        }
     /*
     let cancelbuyOrderConfirmation;
     try {
         console.log("I'm going the check the status of order "+myopenbuyorder.id+"@Bitstamp...");
         cancelbuyOrderConfirmation = await myexchanges.bitstamp.fetchOrder(myopenbuyorder.id, "LTC/EUR");
         console.log("I'm going the check the status of order "+myopenbuyorder.id+"@Bitstamp..."+cancelbuyOrderConfirmation.status);
     */   /*BITSTAMPS order confirmation looks like this
                                                     {
                                                         "id": "958696301",
                                                         "datetime": "2018-02-14T16:52:38.000Z",
                                                         "timestamp": 1518627158000,
                                                         "status": "open",
                                                         "side": "buy",
                                                         "price": 162.19,
                                                         "amount": 0.09,
                                                         "filled": 0,
                                                         "remaining": 0.09,
                                                         "trades": [],
                                                         "info": {
                                                             "status": "Open",
                                                             "id": "958696301",
                                                             "transactions": [],
                                                             "price": "162.19",
                                                             "currency_pair": "LTC/EUR",
                                                             "datetime": "2018-02-14 16:52:38",
                                                             "amount": "0.09000000",
                                                             "type": "0"
                                                         } }
                                                         */    /*     
         } catch(error) {
                         cancelbuyOrderConfirmation = "Error obtaining the Buy Order Confirmation: "+error;
                         console.log(cancelbuyOrderConfirmation); 
                     }*/
}

/**
 * Checks the status of a BTCMarkets order and returns the result
 * @param {*} id 
 */
async function fetchOrderBtcmarkets(id) {
    let orderstatus;
    try {
        console.log("I'm going to check the status of order "+id+"@BTCMarkets...");
        orderstatus = await myexchanges.btcmarkets.fetchOrder(id);
        console.log("I'm going to check the status of order "+id+"@BTCMarkets... Status="+orderstatus.status);
        //console.log(JSON.stringify(orderstatus,null,4));
        /*BTCMARKETS order confirmation looks like this: 
                                                                        {
                                                                            "info": {
                                                                                "id": 1271776254,
                                                                                "currency": "AUD",
                                                                                "instrument": "LTC",
                                                                                "orderSide": "Ask",
                                                                                "ordertype": "Limit",
                                                                                "creationTime": 1518626289870,
                                                                                "status": "Placed",
                                                                                "errorMessage": null,
                                                                                "price": 27272000000,
                                                                                "volume": 9000000,
                                                                                "openVolume": 9000000,
                                                                                "clientRequestId": null,
                                                                                "trades": []
                                                                            },
                                                                            "id": "1271776254",
                                                                            "timestamp": 1518626289870,
                                                                            "datetime": "2018-02-14T16:38:09.870Z",
                                                                            "symbol": "LTC/AUD",
                                                                            "type": "limit",
                                                                            "side": "sell",
                                                                            "price": 272.72,
                                                                            "cost": 24.544800000000002,
                                                                            "amount": 0.09,
                                                                            "filled": 0,
                                                                            "remaining": 0.09,
                                                                            "status": "open",
                                                                            "trades": []
                                                                        }*/
       } catch(error) {
                       let orderstatusmessage;
                       orderstatusmessage = "Error obtaining the Sell Order Confirmation of BTCMarkets: "+error;
                       console.log(orderstatusmessage); 
                       orderstatus = {"status": "Error"}
                       return orderstatus; //This is an object with on the status Error
                       }
    return orderstatus; //This is an object with several components
}

async function cancelBTCMarketsOrder() {
    //CANCEL THE ORDER
    let cancelorderReply2;  
    try{
        console.log("Cancelling BTCMarkets order "+myopensellorder.id+"...");
        cancelorderReply2 = await myexchanges.btcmarkets.cancelOrder(myopensellorder.id);
        console.log("Cancelling BTCMarkets order "+myopensellorder.id+"...Reply:"+cancelorderReply2);
    } catch(error) {console.log("Couldn't cancel my BTCMarket order. Error:"+error);}
    //CONFIRM I CANCELLED THE ORDER
    let cancelsellOrderConfirmation;
    cancelsellOrderConfirmation = await fetchOrderBtcmarkets(myopensellorder.id);
    /*
    try {
         console.log("I'm going the check the status of order "+myopensellorder.id+"@BTCMarkets...");
         cancelsellOrderConfirmation = await myexchanges.btcmarkets.fetchOrder(myopensellorder.id);
         console.log("I'm going the check the status of order "+myopensellorder.id+"@BTCMarkets... Status="+cancelsellOrderConfirmation.status);
         //console.log(JSON.stringify(cancelsellOrderConfirmation,null,4));
         /*BTCMARKETS order confirmation looks like this: 
                                                                         {
                                                                             "info": {
                                                                                 "id": 1271776254,
                                                                                 "currency": "AUD",
                                                                                 "instrument": "LTC",
                                                                                 "orderSide": "Ask",
                                                                                 "ordertype": "Limit",
                                                                                 "creationTime": 1518626289870,
                                                                                 "status": "Placed",
                                                                                 "errorMessage": null,
                                                                                 "price": 27272000000,
                                                                                 "volume": 9000000,
                                                                                 "openVolume": 9000000,
                                                                                 "clientRequestId": null,
                                                                                 "trades": []
                                                                             },
                                                                             "id": "1271776254",
                                                                             "timestamp": 1518626289870,
                                                                             "datetime": "2018-02-14T16:38:09.870Z",
                                                                             "symbol": "LTC/AUD",
                                                                             "type": "limit",
                                                                             "side": "sell",
                                                                             "price": 272.72,
                                                                             "cost": 24.544800000000002,
                                                                             "amount": 0.09,
                                                                             "filled": 0,
                                                                             "remaining": 0.09,
                                                                             "status": "open",
                                                                             "trades": []
                    }*//*
        } catch(error) {
                        cancelsellOrderConfirmation = "Error obtaining the Sell Order Confirmation: "+error;
                        console.log(cancelsellOrderConfirmation); 
                        }
        */

        //Log in DB if BTCMarkets order was canceled correctly, and return "ok" or "not ok"
        //Record IN DB
        if(cancelsellOrderConfirmation.status=="canceled") {
            var sqlQueryOrderDeletedBTCMarkets = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`,`ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`,  `ord_expected_gain`, `ord_exchangeorderid`, `ord_agent`) VALUES ('"+operationID+"','BTCMarkets', 'DELETED-CONFIRMED', 'SELL',  'LTC', 'EUR', '"+pushSellPrice+"', '"+lockedAmountToTrade+"',  '"+pushSellPrice+"', '"+push_percentage+"','"+myopensellorder.id+"', 'ATM_Socket_Limit' );";
            con.query(sqlQueryOrderDeletedBTCMarkets, function (err, result) {if (err) throw err; /*console.log("1 Order Mysql record inserted");*/ });
            return "ok";
        }
        else if(cancelsellOrderConfirmation.status=="closed") { //If the order was closed (executed)
            var sqlQueryOrderDeletedBTCMarkets = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`,`ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`,  `ord_expected_gain`, `ord_exchangeorderid`, `ord_agent`) VALUES ('"+operationID+"','BTCMarkets', 'NOT DELETED-CLOSED', 'SELL',  'LTC', 'EUR', '"+pushSellPrice+"', '"+lockedAmountToTrade+"',  '"+pushSellPrice+"', '"+push_percentage+"','"+myopensellorder.id+"', 'ATM_Socket_Limit' );";
            con.query(sqlQueryOrderDeletedBTCMarkets, function (err, result) {if (err) throw err; /*console.log("1 Order Mysql record inserted");*/ });
            return "not ok - closed instead";
        } 
        else { 
            var sqlQueryOrderDeletedBTCMarkets = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`,`ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`,  `ord_expected_gain`, `ord_exchangeorderid`, `ord_agent`) VALUES ('"+operationID+"','BTCMarkets', 'DELETED-NOTOK', 'SELL',  'LTC', 'EUR', '"+pushSellPrice+"', '"+lockedAmountToTrade+"',  '"+pushSellPrice+"', '"+push_percentage+"','"+myopensellorder.id+"', 'ATM_Socket_Limit' );";
            con.query(sqlQueryOrderDeletedBTCMarkets, function (err, result) {if (err) throw err; /*console.log("1 Order Mysql record inserted");*/ });
            return "not ok";
        }        
    }

/** 
 * Cancels both set of Orders
*/
async function cancelSetOfOrders() {
    let answer1;
    let answer2;
    answer1 = await cancelBitstampOrder(); //Cancel first order
    answer2 = await cancelBTCMarketsOrder(); //Cancel my second order
     //CONFIRM I CANCELLED - Check the status of my orders                                                
     //Check if both orders got canceled correctly
     if(answer1=="ok" && answer2 == "ok") {
         console.log("Both Orders have been succesfully canceled.");
         flag_reachedmypullprice = true;
     }
     else {
             //Send us an email so we operate manually
             var mailOptions = {
                             from: 'mercury@criadoperez.com',
                             to: 'alejandro@criadoperez.com, christian@criadoperez.com, pablo@criadoperez.com',
                             subject: 'Mercury Sockets encounters a problem canceling orders',
                             text: 'Pull level were reached, so I had to cancel the Buy order in Bitstamp and sell order in BTCMarkets. Trying to cancel the order it failed. Please cancel them yourself manually. cancelBitstampOrder said:'+answer1+'. cancelBTCMarketsOrder said:'+answer2
                             }; 
             myemail.myemail.sendMail(mailOptions, function(error, info){
                     if (error) {console.log("Error trying to send email:"+error);} else {console.log('Email sent: ' + info.response);}
                         }); 
             //End of Send email
             //Stop execution
             throw new Error("Something went badly wrong! I'm stopping the execution.");
     }      
}
