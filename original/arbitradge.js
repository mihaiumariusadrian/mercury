//LOAD MY LIBRARIES
'use strict';

require('dotenv').config(); //Load my confidential variables
//Variables can be loaded with process.env.nameofvariable

var trading = false //Sets if executing or not

const myexchanges = require('./myexchanges');
const fetch = require('node-fetch');
var basicAuth = require('express-basic-auth');
var fs = require('fs'); //To read the filesystem and load the SSL Certificates in the server
var https = require('https'); //To have https
var btoa = require('btoa'); //For b64 encoding for btcmarkets api
var nodemailer = require('nodemailer'); //For EMAIL
var tableify = require('tableify'); //convert to html

//Parameters
var baseFiat = "EUR";
var fiats = ["USD", "AUD", "EUR", "ZAR", "IDR", "GBP", "CAD", "JPY", "CZK", "ARS", "MYR", "HKD"];
var baseCryptocurrency = "BTC";
var symbols = ["BTC", "ETH", "BCH","LTC", "XRP"]; //List of accepted cryptocurrencies
var refreshRate = 180; //in seconds
var exchanges = []; 
var ATMMaximumReasonableGainThreshold = 2.00;
var ATMGainThreshold = 1.005; //Higher or equal to this gain it will consider it an opportunity
var ATMGainThresholdInstant = 1.005; //Higher or equal to this gain, it will operate inmediatly at market price
var ATM_Operations_In_Queue = []; //Array that will store my Operations in Queue. Starts Empty.
var listOfAcceptableSymbols = ["BTC/EUR","BTC/AUD","BTC/HKD","BCH/EUR","BCH/AUD","BCH/HKD","LTC/EUR","LTC/AUD","LTC/HKD","XRP/EUR","XRP/AUD","XRP/HKD"];
var balances = []

//RefreshRates (in seconds)
var tickersRefreshRate = 180
var executeStrategyRefreshRate = 5
var fiatDataRefreshRate = 3600
var balancesRefreshRate = 10
var analysisViewRefreshRate = 5

//SET UP EMAIL
/*
  var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: process.env.email_from_notifications,
    pass: 'password'
  }
});
*/
//END SET UP EMAIL

//SET UP DB CONNECTION
var mysql = require('mysql');
var con = mysql.createConnection({
  host: "locahost",
  user: "mercuryDB",
  password: "password",
  database: "mercury"
});
con.connect(function(err) {
   if (err) console.error(err);
   else console.log("Mysql Connected!");
}); 
//END DB SET UP

//DEBUG FUNCTION TO PRINT TESTING INFORMATION
//Print all information of the exchanges for debuging purposes
function debug_show_exchange_information() {
    console.log("PRINTING DEBUG INFORMATION ABOUT ALL MY EXCHANGES")
    for(var i=0; i< exchanges.length;i++) {
        console.log(exchanges[i].id);
        console.log(JSON.stringify(exchanges[i],null,4));
        }
        console.log("END OF PRINTING DEBUG INFORMATION ABOUT ALL MY EXCHANGES")
}

 //FEES OF EXCHANGE
 //I create an array of objects that cotains all our current FEES
 //For the moment we have one fee for all operations, but its ready to add a different fee per operation
 //Ej: Fee BTC/EUR can be different to XRP/EUR
 var ourfees = [
        {name:"Bitstamp"    , operation:"ALL", fee:process.env.bitstamp_fee_all},
        {name:"BTC Markets" , operation:"ALL", fee:process.env.btcmarkets_fee_all},
        {name:"CoinSpot"    , operation:"ALL", fee:process.env.coinspot_fee_all},
        {name:"luno"        , operation:"ALL", fee:process.env.luno_fee_all},
        {name:"CoinMate"    , operation:"ALL", fee:process.env.coinmate_fee_all},
        {name:"Kraken"      , operation:"ALL", fee:process.env.kraken_fee_all},
        {name:"ACX"         , operation:"ALL", fee:process.env.acx_fee_all},
        {name:"CoinBase"    , operation:"ALL", fee:process.env.coinbase_fee_all},
        {name:"Gatecoin"    , operation:"ALL", fee:process.env.gatecoin_fee_all}, 
        {name:"DSX"         , operation:"ALL", fee:process.env.dsx_fee_all}
       ];

//LOAD SSL CERTIFICATE FILES
try{
    var ssl = true
    var key = fs.readFileSync(process.env.SSL_KEY)
    var cert = fs.readFileSync( process.env.SSL_CERT )
    var ca = fs.readFileSync( process.env.SSL_CA)
    
    var ssl_options = {
        key: key,
        cert: cert,
        ca: ca
      }
}
catch (error){
    console.log("Couldn't load SSL")
    var ssl = false
}

//LOAD DATA FROM GOOGLE SHEETS
function readGoogleSheetData() {
    fetch("https://script.google.com/a/criadoperez.com/macros/s/AKfycbz8NwBI299ThPlpLRq8mkKwC4C1p84bAjKzpiwBHiOuT9kELjHF/exe")
    .then(r => {return r})
    .catch(error => {
      console.log(error);
      //return error;
    });
}

//FIAT HANDLER

var fiatData ={"base":"EUR","date":"2018-02-15","rates":{"AUD":1.5757,"BGN":1.9558,"BRL":4.0254,"CAD":1.5604,"CHF":1.1545,"CNY":7.9261,"CZK":25.37,"DKK":7.4493,"GBP":0.8866,"HKD":9.7723,"HRK":7.4383,"HUF":311.68,"IDR":16897.0,"ILS":4.4028,"INR":79.849,"ISK":125.0,"JPY":133.11,"KRW":1330.8,"MXN":23.126,"MYR":4.8654,"NOK":9.735,"NZD":1.6909,"PHP":65.145,"PLN":4.1532,"RON":4.662,"RUB":70.81,"SEK":9.918,"SGD":1.6385,"THB":39.128,"TRY":4.7046,"USD":1.2493,"ZAR":14.561}};

async function updateEURFIATData(){
    const url = "https://api.fixer.io/latest?base=EUR";
    fetch(url)
    .then(
            function(response) { 
                        //First check if the status is 200 which means its OK
                        if (response.status !== 200) {
                        console.log('Looks like there was a problem. Status Code: ' +
                        response.status);
                        return;
                        }
                    // Examine the text in the response
                    response.json().then(function(data) {
                            //console.log("MI DATA..."+data.base);
                            console.log("ANTES:"); console.log(fiatData);
                            fiatData = data;
                            console.log("DESPUES:"); console.log(fiatData);
                            console.log("UPDATED FIAT DATA succesfully");
                    });
    })
    .catch(error => {
      console.log("Catch ERROR updating EURFIATDATA: "+error);
      return false;
    });
}

function fiatRate(base, to){
    if(base==to) return 1
    return fiatData.rates[to]
}

function fetchSymbols(exchange, symbol){
    try{
        var symbols = []
        exchange.loadMarkets();
        for(var i=0;i<exchange.symbols.length;i++){
            let s = exchange.symbols[i].split('/');
            if(s[0]==symbol || s[1]==symbol){
                symbols.push(exchange.symbols[i]);
            }
        }
        return symbols;
    }
    catch(error){
        return error;
    }
}

function calculateGains(ticker1, ticker2){
    if(ticker1[baseFiat] != null && ticker2[bacseFiat] != null) return ticker2[baseFiat]/ticker1[baseFiat]
}


async function getBalances(exchanges){
    var balances = []
    for(var i=0;i<exchanges.length;i++){
        await(exchanges[i].fetchBalance ()).then((response)=>{
            //console.log(exchanges[i].name +" balance: ", response)
            let balance = {
                name: exchanges[i].name,
                free: response.free,
                used: response.used
            }
        balances.push(balance)
        }).catch((error)=>{
            console.log(error)
            return error})
       }
    return balances;
} 

//FUNCTION THAT RETURNS THE FEES WE HAVE IN BROKER ACCORDING TO OUR TABLE
function getOurFees(exchangeToGetFeeFrom){
         //TRY TO GET THE FEE OF THE BROKER. If I cant find it use value 1 (0% fees)
         let applicablefee;
         try {
            applicablefee = ourfees.find(o => o.name === exchangeToGetFeeFrom && o.operation ==='ALL').fee;
            console.log("Loading exchange "+exchangeToGetFeeFrom+". Applicable Fee: "+applicablefee);
            return applicablefee;
         }
          catch(err) {
            applicablefee = 1;
            console.log("Loading exchange "+exchangeToGetFeeFrom+" APPLICABLE FEE NOT FOUND. USING VALUE 1.00 (0% FEE)");
            return applicablefee;
          }
          

}

async function fetchData(exchanges){
    var tickers= [];
    for (let exchange of exchanges){ //For each exchange

         //For the moment this gets the standard fee of operation "all" from the specific broker
         let applicablefee;
         //TRY TO GET THE FEE O THE BROKER. If I cant find it use value 1 (0% fees)
         /*try {
            applicablefee = ourfees.find(o => o.name === exchange.name && o.operation ==='ALL').fee;
            console.log("Loading exchange "+exchange.name+". Applicable Fee: "+applicablefee);
         }
          catch(err) {
            applicablefee = 1;
            console.log("Loading exchange "+exchange.name+" APPLICABLE FEE NOT FOUND. USING VALUE 1.00 (0% FEE)");
            //fee_exchange: 1
          }*/
          applicablefee = getOurFees(exchange.name);

        let listOfSymbols = exchange.symbols;
        //console.log("List of Symbols: "+listOfSymbols);
        //For each symbol in each exchange...
        if(listOfSymbols != null){
            for(var i=0; i<listOfSymbols.length; i++){
                //only get ticker for my acceptable symbols
                 if(listOfAcceptableSymbols.find(o => o == listOfSymbols[i] ) == undefined) {
                            //do nothing (ignore this symbol)
                            console.log(listOfSymbols[i]+" not downloading.");
                 }
                 //If is in my list of acceptable symbols I fetch the Ticker
                 else {                   
                        let ticker = await (exchange.fetchTicker(listOfSymbols[i])).then(
                            (ticker) => {
                                let customTicker = {
                                    exchange: exchange.name,
                                    symbol: ticker.symbol,
                                    symbol1: ticker.symbol.split('/')[0],
                                    symbol2: ticker.symbol.split('/')[1],
                                    last: ticker.last,
                                    bid: ticker.bid,
                                    ask: ticker.ask,
                                    //baseVolume: ticker.baseVolume,
                                    [baseFiat]: ticker.last/fiatRate(baseFiat, ticker.symbol.split('/')[1]),
                                    [baseCryptocurrency]: -1,
                                    fee_exchange: applicablefee
                                }
            
                                tickers.push(customTicker);
                                //console.log(exchange.name)
                                console.log(ticker.symbol +" "+ticker.last);
                                //console.log(ticker.last)
                            }
                        ).catch((error)=>{
                            console.log(error)
                            return error})
                    }
            } 
        }
        
    }
    return tickers
}

/*******
*
* ATM -> Automatic Trading Machine. It's formed by 3 stages
*       ATM_Evaluate
*       ATM_Prioritize
*       ATM_Execute
*
*       ATM_Evaluate
*       ATM_Evaluate a possible operation and executes it the following conditions are met
*       1. Gain is over a certain %
*       2. Final destination is an acceptable FIAT (EUR or AUD)
*       3. Balance is avaible in the selling side and buying side
*       Returns as object of status and reason. Ex: ["APPROVED",''] if Approved. If not, it sends an array with the reason ["DENIED",'Low gain']
*
*       ATM_Prioritize
*       Order my candidates to operate in order of most profitable
*   
*       ATM_Execute Execute my best Operation
*       ATM_Execute_Instant -> Execute instantly with Market Price
*       ATM_Execute_Limit -> Execute instantly with Limit Price
*       
****/
function ATM_Evaluate(operationToConsider) {
    var ATM_approval = ["DENIED", "By default"]; //ATM_approval is an array of status and reason. By default operation is DENIED
    var balanceOfSymbolBaseToConsider = 0; //By default balance is 0
    var balanceOfSymbolEndToConsider = 0; //By default balance is 0

    //Example: Buy BCH@Bitstamp(EUR) --> Sell BCH@BTCMarkets(AUD). Make 3.25%. APPROVED
    var message2 = "ATM_Evaluate is considering an operation: Buy "+operationToConsider.base.symbol1+"@"+operationToConsider.base.exchange+"("+operationToConsider.base.symbol2+") --> Sell "+operationToConsider.end.symbol1+"@"+operationToConsider.end.exchange+"("+operationToConsider.end.symbol2+") to make "+operationToConsider.gain+"%...";
    //console.log(message2);


    //1ST CONDITION. GAIN IS OVER A CERTAIN PERCENTAGE AND BELOW A REASONABLE PERCENTAGE
    if(!(operationToConsider.gain > ATMGainThreshold)) {
                                            ATM_approval = ["DENIED", "Low gain"]; 
                                            //console.log("\t"+ATM_approval[0]+" "+ATM_approval[1]);
                                            return ATM_approval; //If the operation is already denied dont bother to continue ATM
                                        }
    else if (operationToConsider.gain > ATMMaximumReasonableGainThreshold) {
                                            ATM_approval = ["DENIED", "Gain to high"];
                                            //console.log("\t"+ATM_approval[0]+" "+ATM_approval[1]);
                                            return ATM_approval; //If the operation is already denied dont bother to continue ATM
    }   
    else                               {
                                        //console.log("\tGain acceptable. Let's check currency destination...");
                                    }


    //2ND CONDITION. If final destination is EUR or AUD accept. If not DENY
    if (operationToConsider.end.symbol2=='EUR') { console.log("\tFinal currency destination is ok. It's EUR."); }
    else if (operationToConsider.end.symbol2=='AUD') { console.log("\tFinal currency destination is ok. It's AUD.");}
    else {
        ATM_approval=["DENIED", "Final destination is not EUR or AUD. Consider the operation manually"];
        console.log("\t"+ATM_approval[0]+" "+ATM_approval[1]);
        return ATM_approval; //If final currency destionation is not acceptable don't continue ATM
    }

    //3RD CONDITION. Check if Im using an approved exchange
    //If its an approved exchange continue. If not, deny with cause "UNAPPROVED EXCHANGE" and send email
    let approvedExchanges = ["BTC Markets","Kraken","Bitstamp", "ACX"];
    if(approvedExchanges.indexOf(operationToConsider.base.exchange) == '-1' || approvedExchanges.indexOf(operationToConsider.end.exchange) == '-1') {
                //Send us an email so we operate manually
                var mailOptions2 = {
                    from: process.env.email_from_notifications,
                    to: process.env.email_to_notifications,
                    subject: 'Mercury detected an opportunity in an UNAPPROVED EXCHANGE',
                    text: 'I detected an opportunity in an UNAPPROVED EXCHANGE. That means I cant operate myself but you can do it manually. If you can please do!:'+message2
                  }; 
                transporter.sendMail(mailOptions2, function(error, info){
                  if (error) {
                    console.log("Error trying to send email:"+error);
                  } else {
                    console.log('Email sent: ' + info.response);
                  }
                }); 
                //End of Send email
        ATM_approval=["DENIED", "Unapproved exchange"];
        console.log("\t"+ATM_approval[0]+" "+ATM_approval[1]);
        return ATM_approval;
    }



    //3RD CONDITION. Check if I have balance to make the operation
    console.log("\tChecking if I have balance to complete the operation. I need "+operationToConsider.base.symbol2+" in "+operationToConsider.base.exchange+"  and "+operationToConsider.end.symbol1+" in "+operationToConsider.end.exchange);  
    let exchangeToConsider1 = exchanges.find(exchange => exchange.name == operationToConsider.base.exchange);
    let exchangeToConsider2 = exchanges.find(exchange => exchange.name == operationToConsider.end.exchange);
    //Get balance of what I'm buying with where I want to buy in (BASE SIDE)

     balanceOfSymbolBaseToConsider = balances.find(balance =>{ return balance.name == operationToConsider.base.exchange}).free[operationToConsider.base.symbol2]; //I store in a variable the balance of my symbol
     //console.log("     Free Balance "+operationToConsider.base.symbol2+" in "+operationToConsider.base.exchange+": "+balanceOfSymbolBaseToConsider);                                                
    //Get balance of what I'm selling where I what to sell in (END SIDE)
    balanceOfSymbolEndToConsider = balances.find(balance => {return balance.name === operationToConsider.end.exchange}).free[operationToConsider.end.symbol1]; //I store in a variable the balance of my symbol
    //console.log("\tFree Balance "+operationToConsider.end.symbol1+" in "+operationToConsider.end.exchange+": "+balanceOfSymbolEndToConsider);         
     //If I also have balance on both sides to operate then APPROVE the operation
     //Also returns the amount that has been approved.
    if(balanceOfSymbolBaseToConsider > 0 && balanceOfSymbolEndToConsider > 0) {  console.log("\tI have some balance to operate. Let me see the volume I can approve..."); }
            console.log("\tI have some balance to operate. Let me see the volume I can approve...");

    //To be able to trade the lockedAmountToTrade I need the following condition to work
    var lockedAmountToTrade;
    switch(operationToConsider.end.symbol1) {
        case 'BTC': lockedAmountToTrade=0.0025; break;
        case 'BCH': lockedAmountToTrade=0.02; break;
        case 'XRP': lockedAmountToTrade=20; break;
        case 'LTC': lockedAmountToTrade=0.12; break;
        case 'ETH': lockedAmountToTrade=0.03; break;
        default: lockedAmountToTrade='0'
    }
    if(balanceOfSymbolEndToConsider >= lockedAmountToTrade && balanceOfSymbolBaseToConsider >= lockedAmountToTrade * operationToConsider.base.ask * 1.10) {
        //Math.min(balanceOfSymbolEndToConsider, lockedAmountToTrade);
        ATM_approval= ["APPROVED","I have enough balance to operate with a margin error of 10%"];
        console.log("\t"+ATM_approval[0]+" "+ATM_approval[1]);
        return ATM_approval; //Returned approved if everything is ok
    }        
       
    else {
        ATM_approval = ["DENIED", "Not enough balance to trade"]; 
        console.log("\t"+ATM_approval[0]+" "+ATM_approval[1]);
        //Send us an email so we move coins
             var mailOptions2 = {
                from: process.env.email_from_notifications,
                to: process.env.email_to_notifications,
                subject: 'Mercury needs you to move coins',
                text: 'I can make money but couldnt trade because the coins are not in the right place. Move them for me please!:'+message2
              }; 
            transporter.sendMail(mailOptions2, function(error, info){
              if (error) {
                console.log("Error trying to send email:"+error);
              } else {
                console.log('Email sent: ' + info.response);
              }
            }); 
            //End of Send email

        return ATM_approval; //If I have no balance dont continue ATM
    }  

}

//This function looks at all the Operations in my Queue and order them to execute first to most profitable one
//It should also check the properties of the selected Exchange to decide what type of ATM_Execute I should use for it. (Example if it doesnt accept market orders, dont do them)
/*async function ATM_Prioritize() {

    //If the Queue of operations is empty, return, there is nothing to do
    if(ATM_Operations_In_Queue.length == 0) { console.log("ATM Stopped. There are  no profitable operations I can do."); return;} 
    //If not continue
    console.log("ATM_Prioritize has "+ATM_Operations_In_Queue.length+" operations in the queue that can make money.");
    //Refresh my Operations data to confirm they are still good
    for (var i = 0; i < ATM_Operations_In_Queue.length; i++) {
        //ATM_Operations_In_Queue[i] = await reCalculateOperation(ATM_Operations_In_Queue[i]);
    
       //Record in database the Oportunities
       //Would be nice if it records new gain and old to test if there is a difference 
       var sqlQuery_OpportunitiesInsert = "INSERT INTO `mercury`.`atm_opportunities` (`opp_exchange1`, `opp_exchange2`, `opp_coin`,`opp_gain`, `opp_operationid`) VALUES ('"+ATM_Operations_In_Queue[i].base.exchange+"', '"+ATM_Operations_In_Queue[i].end.exchange+"', '"+ATM_Operations_In_Queue[i].base.symbol1+"', '"+ATM_Operations_In_Queue[i].gain+"', '"+ATM_Operations_In_Queue[i].operationID+"')";
       con.query(sqlQuery_OpportunitiesInsert, function (err, result) {
                   if (err) throw err;
                  // console.log("1 atm_opportunities record inserted "+result.insertId);
                 });
      
    } 
    //I order my array of Operations from highest to lowest gain
    function compare(a,b) {
        if (a.gain > b.gain)
          return -1;
        if (a.gain < b.gain)
          return 1;
        return 0;
      }
      //Order the objects in my array by gain
      ATM_Operations_In_Queue.sort(compare);

      //Check if my Operation has the requirements to trade with ATM_Execute_Instant (Doesnt have the flag of createMarketOrder set to false) and stay in the highest position that is compatible.
      var ATM_Execute_Instant_Compatible = false; 
      var iwhile = 0;
      while((ATM_Execute_Instant_Compatible == false) &&  (iwhile < ATM_Operations_In_Queue.length)) {
            console.log("Position "+iwhile+" has a gain of "+ATM_Operations_In_Queue[iwhile].gain);
            let exchangeP1 = exchanges.find(exchange => exchange.name == ATM_Operations_In_Queue[iwhile].base.exchange);
            let exchangeP2 = exchanges.find(exchange => exchange.name == ATM_Operations_In_Queue[iwhile].end.exchange);
            //console.log("exchangeP1.has.fetchOrder: "+exchangeP1.has.fetchOrder);
            //console.log("exchangeP2.has.fetchOrder: "+exchangeP2.has.fetchOrder);     
            //console.log("Print object exchangeP1.has..."+JSON.stringify(exchangeP1.has,null,4));
            //console.log("Print object exchangeP2.has..."+JSON.stringify(exchangeP2.has,null,4));
            //If any of the 2 brokers involved as market as that it can't accept Market Order, then it is not ATM_Execute_Instant_Compatible
            if(exchangeP1.has.createMarketOrder == false || exchangeP2.has.createMarketOrder == false) {
                console.log("One or both exchanges cant accept Market Orders.")
                console.log("Operation in ATM_Exchange_Instant["+iwhile+"] IS NOT compatible. OperationID"+ATM_Operations_In_Queue[iwhile].operationID+" Gain:"+ATM_Operations_In_Queue[iwhile].gain);
                //UPDATE DATABASE TO MARK OPPORTUNIY AS NOT COMPATIBLE
                var sqlQuery_OpportunitiesInsert = "UPDATE `mercury`.`atm_opportunities` SET `opp_atmexecuteinstantcompatible`='NO' WHERE `opp_operationid`='"+ATM_Operations_In_Queue[iwhile].operationID+"'";
                con.query(sqlQuery_OpportunitiesInsert, function (err, result) {if (err) throw err; });
            }
           else {
                console.log("Operation is ATM_Exchange_Instant["+iwhile+"] IS compatible. OperationID:"+ATM_Operations_In_Queue[iwhile].operationID+" Gain:"+ATM_Operations_In_Queue[iwhile].gain);
                ATM_Execute_Instant_Compatible = true;
                //UPDATE DATABASE TO MARK OPPORTUNIY AS COMPATIBLE
                var sqlQuery_OpportunitiesInsert = "UPDATE `mercury`.`atm_opportunities` SET `opp_atmexecuteinstantcompatible`='YES' WHERE `opp_operationid`='"+ATM_Operations_In_Queue[iwhile].operationID+"'";
                con.query(sqlQuery_OpportunitiesInsert, function (err, result) {if (err) throw err; });
             }
           
           /*
            if(exchangeP1.has.fetchOrder && exchangeP2.has.fetchOrder) {
                    console.log("Operation is ATM_Exchange_Instant["+iwhile+"] IS compatible. OperationID:"+ATM_Operations_In_Queue[iwhile].operationID);
                    ATM_Execute_Instant_Compatible = true;
                    //UPDATE DATABASE TO MARK OPPORTUNIY AS COMPATIBLE
                    var sqlQuery_OpportunitiesInsert = "UPDATE `mercury`.`atm_opportunities` SET `opp_atmexecuteinstantcompatible`='YES' WHERE `opp_operationid`='"+ATM_Operations_In_Queue[iwhile].operationID+"'";
                    con.query(sqlQuery_OpportunitiesInsert, function (err, result) {if (err) throw err; });
                }
            else {
                console.log("Operation in ATM_Exchange_Instant["+iwhile+"] IS NOT compatible. OperationID"+ATM_Operations_In_Queue[iwhile].operationID);
                //UPDATE DATABASE TO MARK OPPORTUNIY AS NOT COMPATIBLE
                var sqlQuery_OpportunitiesInsert = "UPDATE `mercury`.`atm_opportunities` SET `opp_atmexecuteinstantcompatible`='NO' WHERE `opp_operationid`='"+ATM_Operations_In_Queue[iwhile].operationID+"'";
                con.query(sqlQuery_OpportunitiesInsert, function (err, result) {if (err) throw err; });
            }*/
            /*
            iwhile++;
        }
      var i_to_execute = iwhile - 1; //i_to_execute indicates the first operation in my array that is compatible with ATMExecute_Instant
      //Check if Gain is over a than mi Instant Thershold (around 3%) it will operate Instantly, (Market Price)
      if(ATM_Operations_In_Queue[i_to_execute].gain >= ATMGainThresholdInstant) {
          await ATM_Execute_Instant(ATM_Operations_In_Queue[i_to_execute]);
      }
      else { //If not then trade another way (PENDING WORK)
            //console.log(JSON.stringify(ATM_Operations_In_Queue[0],null,4));
            console.log("ATM_Execute_Instant wont trade. The detected opportunity is below the ATMGainThresholdInstant of "+(((ATMGainThresholdInstant)-1)*100)+"%");
            //console.log("ATM_Prioritize is going to execute the one with the highest gain...");
            //ATM_Evaluate(ATM_Operations_In_Queue[0]);//This line is optional. Just in case RE-evaluate and also display data in the console.
            //await ATM_Execute_Limit(ATM_Operations_In_Queue[0]);
        }

}
*/
/*
async function ATM_Execute_Instant(operationToExecute) { //PENDING WORK. If it trades the lockedAmount, after instead of not doing anything, it should reevalute and if gain is still there, execute again.

    //Execute Operation at Market Price
    console.log("ATM_Execute_Instant is considering to trade operationID "+operationToExecute.operationID);
    //Re-download data and confirm margin is still good
    operationToExecute = await reCalculateOperation(operationToExecute);
    console.log("Operation to execute refreshed."); 

    if(operationToExecute.gain >= ATMGainThresholdInstant) {
        console.log("ATM_Execute_Instant is going to trade... Expected gain is "+operationToExecute.gain);
            //Example: Buy BCH@Bitstamp(EUR) --> Sell BCH@BTCMarkets(AUD). Make 3.25%. APPROVED
        var message1 = "ATM_Execute_Instant is going to trade...: Amount: "+operationToExecute.volume+operationToExecute.base.symbol1+".  Buy "+operationToExecute.base.symbol1+"@"+operationToExecute.base.exchange+"("+operationToExecute.base.symbol2+") --> Sell "+operationToExecute.end.symbol1+"@"+operationToExecute.end.exchange+"("+operationToExecute.end.symbol2+") to make "+operationToExecute.gain+"%...";
        console.log(message1);

        let exchangeOperating1 = exchanges.find(exchange => exchange.name == operationToExecute.base.exchange);
        let exchangeOperating2 = exchanges.find(exchange => exchange.name == operationToExecute.end.exchange);
        //console.log(JSON.stringify(exchangeOperating1,null,4));
        //console.log(JSON.stringify(exchangeOperating2,null,4));

        //Send email before I trade
        var mailOptions1 = {
            from: process.env.email_from_notifications,
            to: process.env.email_to_notifications,
            subject: 'Mercury Instant Operation',
            text: 'ATM_Execute_Instant tried to do:'+message1
          };
          
        transporter.sendMail(mailOptions1, function(error, info){
          if (error) {
            console.log("Error trying to send email:"+error);
          } else {
            console.log('Email sent: ' + info.response);
          }
        }); 
        //End of Send email


        //Send Market Order on Exchange1
        var mybuyorder;
        try { mybuyorder = await exchangeOperating1.createOrder(operationToExecute.base.symbol,'market','buy',operationToExecute.volume); //Ex: createLimiBuyOrder(BCH/EUR,market,buy,1.5)

       } catch(error) { console.log("Something went wrong. I couldnt complete the BUY order!!"+error);  }
    
        //Send Market Order on Exchange2
        var mysellorder;
        try {
            mysellorder = await exchangeOperating2.createOrder(operationToExecute.end.symbol,'market','sell',operationToExecute.volume); //Ex: createLimiBuyOrder(BCH/EUR,market,buy,1.5)
        }  catch(error) { console.log("Something went wrong. I couldnt complete the SELL order!!"+error);  }
        console.log("DEBUG POINT ALPHA");
        //Lets try and confirm I made the operation. Print confirmation on the console
        let buyOrderConfirmation;
        let sellOrderConfirmation;
        try {
            buyOrderConfirmation = exchangeOperating1.fetchOrder(mybuyorder.id);
            console.log(JSON.stringify(buyOrderConfirmation,null,4));
            } catch(error) {
                            buyOrderConfirmation = "Error obtaining the Buy Order Confirmation: "+error;
                            console.log(buyOrderConfirmation); 
                        }
        try {
             sellOrderConfirmation = exchangeOperating2.fetchOrder(mysellorder.id);
             console.log(JSON.stringify(sellOrderConfirmation,null,4));
        } catch(error) {
                         sellOrderConfirmation = "Error obtaining the Sell Order Confirmation: "+error;
                         console.log(sellOrderConfirmation); 
                         }
        console.log("DEBUG POINT BETA");
        //IF the order returned something, log it in the DATABASE
         if(typeof mybuyorder != "undefined") {
                //Record order1 in DB WITH ORDERID AND STATUS SENT
                var sqlQueryOrdersBuyInsert = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`, `ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`,  `ord_expected_gain`, `ord_exchangeorderid`, `ord_agent`) VALUES ('"+operationToExecute.operationID+"', '"+operationToExecute.base.exchange+"', 'SENT', 'BUY',  '"+operationToExecute.base.symbol1+"', '"+operationToExecute.base.symbol2+"', 'market', '"+operationToExecute.volume+"',  '"+operationToExecute.baseAsk+"',       '"+operationToExecute.gain+"','"+mybuyorder.id+"', 'ATM_Execute_Instant' );";
                console.log("DEBUG 1: "+sqlQueryOrdersBuyInsert);
                con.query(sqlQueryOrdersBuyInsert, function (err, result) {if (err) throw err; console.log("1 Order record inserted"+result.insertId); });       
                }
        else {
                //Record order1 in DB WITH NO ORDERID AND STATUS UNCONFIRMED
                var sqlQueryOrdersBuyInsert = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`, `ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`, `ord_expected_gain`, `ord_agent`) VALUES ('"+operationToExecute.operationID+"', '"+operationToExecute.base.exchange+"', 'UNCONFIRMED', 'BUY',  '"+operationToExecute.base.symbol1+"', '"+operationToExecute.base.symbol2+"', 'market', '"+operationToExecute.volume+"','"+operationToExecute.baseAsk+"','"+operationToExecute.gain+"', 'ATM_Execute_Instant' );";
                console.log("DEBUG 2: "+sqlQueryOrdersBuyInsert);
                con.query(sqlQueryOrdersBuyInsert, function (err, result) {if (err) throw err; console.log("1 Order record inserted UNCONFIRMED"); });       

        }
         if(typeof mysellorder != "undefined") {
                //Record order2 in DB WITH ORDERID AND STATUS SENT
                var sqlQueryOrdersSellInsert = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`, `ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`, `ord_expected_gain`, `ord_exchangeorderid`, `ord_agent`) VALUES ('"+operationToExecute.operationID+"', '"+operationToExecute.end.exchange+"', 'SENT', 'SELL',  '"+operationToExecute.end.symbol1+"', '"+operationToExecute.end.symbol2+"', 'market', '"+operationToExecute.volume+"','"+operationToExecute.endBid+"', '"+operationToExecute.gain+"','"+mysellorder.id+"', 'ATM_Execute_Instant' );";
                console.log("DEBUG 3: "+sqlQueryOrdersSellInsert);
                con.query(sqlQueryOrdersSellInsert, function (err, result) {if (err) throw err; console.log("1 Order record inserted"+result.insertId); });
                        }
        else {
                //Record order2 in DB WITH NO ORDERID AND STATUS UNCONFIRMED
                var sqlQueryOrdersSellInsert = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`, `ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`, `ord_expected_gain`, `ord_agent`) VALUES ('"+operationToExecute.operationID+"', '"+operationToExecute.end.exchange+"', 'UNCONFIRMED', 'SELL',  '"+operationToExecute.end.symbol1+"', '"+operationToExecute.end.symbol2+"', 'market', '"+operationToExecute.volume+"','"+operationToExecute.endBid+"', '"+operationToExecute.gain+"', 'ATM_Execute_Instant' );";
                console.log("DEBUG 4: "+sqlQueryOrdersSellInsert);
                con.query(sqlQueryOrdersSellInsert, function (err, result) {if (err) throw err; console.log("1 Order record inserted UNCONFIRMED"); });
        }

    } else {
        console.log("To slow. Operation is no longer profitable. ATM_Execute_Instant is standing down and not trading.")
         //Record order 1 and order2 in DB AS RETREAT, because I wont do them at the end. (After the recalculation it stopped being profitable)
         var sqlQueryOrdersSellInsert = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`, `ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`, `ord_expected_gain`, `ord_agent`) VALUES ('"+operationToExecute.operationID+"', '"+operationToExecute.end.exchange+"', 'RETREAT', 'SELL',  '"+operationToExecute.end.symbol1+"', '"+operationToExecute.end.symbol2+"', 'market', '"+operationToExecute.volume+"','"+operationToExecute.endBid+"', '"+operationToExecute.gain+"', 'ATM_Execute_Instant' );";
         console.log("DEBUG 5A: "+sqlQueryOrdersSellInsert);
         con.query(sqlQueryOrdersSellInsert, function (err, result) {if (err) throw err; console.log("1 Order record inserted RETREAT"); });
         var sqlQueryOrdersBuyInsert = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`, `ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`, `ord_expected_gain`, `ord_agent`) VALUES ('"+operationToExecute.operationID+"', '"+operationToExecute.base.exchange+"', 'RETREAT', 'BUY',  '"+operationToExecute.base.symbol1+"', '"+operationToExecute.base.symbol2+"', 'market', '"+operationToExecute.volume+"','"+operationToExecute.baseAsk+"', '"+operationToExecute.gain+"', 'ATM_Execute_Instant' );";
         console.log("DEBUG 5B: "+sqlQueryOrdersBuyInsert);
         con.query(sqlQueryOrdersBuyInsert, function (err, result) {if (err) throw err; console.log("1 Order record inserted RETREAT"); });       
         
         
    }
}
*/
async function mercuryLimitOrder(exchange,symbol, limitPrice, side, volume ) { //PENDING WORK. If it trades the lockedAmount, after instead of not doing anything, it should reevalute and if gain is still there, execute again.        //Send email before I trade
     //Send Market Order on Exchange
     let order = await (exchange.createOrder(symbol,side,"limit", volume, limitPrice)) 
     .catch(error =>{ console.log("Something went wrong. I couldnt complete the order!!"+error);
                     return error})
    //Send email about ourder 
    var mailOptions1 = {
                from: process.env.email_from_notifications,
                to: process.env.email_to_notifications,
                subject: 'Mercury Instant Operation',
                text: 'Limit Order sent: '+ exchange +" price: "+limitPrice+" tpye:"+type+" volume: "+ volume + "order" + order
    };    
    transporter.sendMail(mailOptions1, function(error, info){
              if (error) {
                console.log("Error trying to send email:"+error);
              } else {
                console.log('Email sent: ' + info.response);
              }
    }); 
 
    //IF the order returned something, log it in the DATABASE
    if(typeof order != "undefined") {
       //Record order1 in DB WITH ORDERID AND STATUS SENT
        let sqlQueryOrdersBuyInsert = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`, `ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`,  `ord_expected_gain`, `ord_exchangeorderid`, `ord_agent`) VALUES ('"+getFormattedDate()+"-"+Math.random().toString(36).substr(2, 9)+"', '"+exchange.name+"', 'SENT', '"+ side+"',  '"+symbol.split('/')[0]+"', '"+symbol.split('/')[1]+"', 'limit', '"+volume+"',  '"+limitPrice+"',  '"+0+"','"+mybuyorder.id+"', 'ATM_Execute_Instant' );";
        console.log("DEBUG 1: "+sqlQueryOrdersBuyInsert);
        con.query(sqlQueryOrdersBuyInsert, function (err, result) {if (err) throw err; console.log("1 Order record inserted"+result.insertId); });       
    }
    else {
    //Record order1 in DB WITH NO ORDERID AND STATUS UNCONFIRMED
        let sqlQueryOrdersBuyInsert = "INSERT INTO `mercury`.`atm_orders` (`ord_operationid`, `ord_exchange`, `ord_status`, `ord_optype`, `ord_symbol1`, `ord_symbol2`, `ord_price`, `ord_volume`, `ord_expectedprice`, `ord_expected_gain`, `ord_agent`) VALUES ('"+getFormattedDate()+"-"+Math.random().toString(36).substr(2, 9)+"', '"+exchange.name+"', 'UNCONFIRMED', '"+side+ "',  '"+symbol.split('/')[0]+"', '"+symbol.split('/')[1]+"', 'limit', '"+volume+"','"+limitPrice+"','"+0+"', 'ATM_Execute_Instant' );";
        console.log("DEBUG 2: "+sqlQueryOrdersBuyInsert);
        con.query(sqlQueryOrdersBuyInsert, function (err, result) {if (err) throw err; console.log("1 Order record inserted UNCONFIRMED"); });       
    }
    console.log("Performed Order: ")
    console.log(order)
    return order
}

/******
 * I HAVE 3 KINDS OF OPERATIONS: Open (buy, transfer and sell), Locked (with stock) or Closed (loops). I only accepted Locked operations for the moment
*  PENDING WORK. Here it should see if the Operation is Open, Locked or Closed and execute the appropiate process. Currently only working for Locked Operations.
 */
 //Function that completes a LIMIT LOCKED Operation
async function ATM_Execute_Limit(operationToTradeLocked) {
    //Execute Operation with Limit Orders
    console.log("ATM_Execute_Limit is going to trade...");
        //Store in variables the arguments of the Object Operation I'm going to use
        var TLExchangeStart = operationToTradeLocked.base.exchange;
        var TLExchangeEnd =  operationToTradeLocked.end.exchange;
        var TLBuyStart = operationToTradeLocked.base.symbol;
        var TLSellEnd = operationToTradeLocked.end.symbol;
    
        //Store in a variable the Exchanges I'm goin to use
        let exchangeToOperateStart = operationToTradeLocked.base.exchange.name;
        let exchangeToOperateEnd = operationToTradeLocked.end.exchange.name;
        let exchangeToOperateEnd2 = exchanges.find(exchange => exchange.name == TLExchangeStart); 
    
        //Example: Buy BCH@Bitstamp(EUR) --> Sell BCH@BTCMarkets(AUD). Make 3.25%. APPROVED
        console.log("ATM_Execute_Limit is reconsidering an operation: Buy "+operationToTradeLocked.base.symbol1+"@"+operationToTradeLocked.base.exchange+"("+operationToTradeLocked.base.symbol2+") --> Sell "+operationToTradeLocked.end.symbol1+"@"+operationToTradeLocked.end.exchange+"("+operationToTradeLocked.end.symbol2+") to make "+operationToTradeLocked.gain+"%...");
        //THIS IS WRONG, BUT FIX IT  
        //var myorderbookb = await exchangeToOperateEnd2.fetchL2OrderBook(TLSellEnd);
        //console.log(JSON.stringify(myorderbookb,null,4));
    
        //Calculate the amount I want to trade. For the moment its the static value depending on the currency
        var lockedAmountToTrade;
        switch(operationToTradeLocked.base.symbol1) {
            case 'BTC': lockedAmountToTrade=0.0025; break;
            case 'BCH': lockedAmountToTrade=0.02; break;
            case 'XRP': lockedAmountToTrade=20; break;
            case 'LTC': lockedAmountToTrade=0.12; break;
            case 'ETH': lockedAmountToTrade=0.03; break;
            default: lockedAmountToTrade='0'
        }  
        console.log("\tStarting to Trade a Locked operation of "+lockedAmountToTrade+" "+TLBuyStart+"@"+TLExchangeStart+" to "+TLExchangeEnd);
        try{
            console.log("\tFake Buying "+lockedAmountToTrade+TLBuyStart+" in "+TLExchangeStart+"...");
            //await.exchangeToOperateStart.createLimitBuyOrder(TLBuyStart,lockedAmountToTrade);
            //Sell at Market Price
            console.log("\tFake Selling "+lockedAmountToTrade+TLSellEnd+" at Market Price in "+TLExchangeEnd+"...");
             //await.exchangeToOperateEnd.createOrder(TLSellEnd,'market','sell',lockedAmountToTrade); //Ex: createLimiBuyOrder(BCH/EUR,market,buy,1.5)
            console.log("\tOperation completed")
        }
        catch(error) {
            console.error("Something went wrong. I couldnt complete the LOCKED Operation..."+error);
        }
    
}

//You give him an Operation and downloads fresh data of the same Operation
async function reCalculateOperation (oldOperation) {
    //console.log("Recalculation Operation...");
    //console.log(JSON.stringify(oldOperation,null,4));
    //console.log("oldOperation.base.exchange"+oldOperation.base.exchange)
    //console.log("oldOperation.end.exchange"+oldOperation.end.exchange)
    let exchangerc1 = exchanges.find(exchange => exchange.name == oldOperation.base.exchange);
    let exchangerc2 = exchanges.find(exchange => exchange.name == oldOperation.end.exchange);
    //console.log("Print object exchangerc1..."+JSON.stringify(oldOperation,null,4));
    let tickerrc1 = await exchangerc1.fetchTicker(oldOperation.base.symbol); //Need to add try-catch
    let tickerrc2 = await exchangerc2.fetchTicker(oldOperation.end.symbol); //Need to add try-catch
    //console.log("Pring object tickerrc1..."+JSON.stringify(tickerrc1,null,4));

    //Calculate Volume of the Operation to right in the Operation Object.
    //PENDING FOR IMPROVEMENT. At the moment this is fixed amount.
    var lockedAmountToTrade;
    switch(oldOperation.base.symbol1) {
        case 'BTC': lockedAmountToTrade=0.0025; break;
        case 'BCH': lockedAmountToTrade=0.02; break;
        case 'XRP': lockedAmountToTrade=20; break;
        case 'LTC': lockedAmountToTrade=0.12; break;
        case 'ETH': lockedAmountToTrade=0.03; break;
        default: lockedAmountToTrade='0'
    } 


    //Customize tickerrc1 to my custom format
    let custom_tickerrc1 = {
                    exchange: oldOperation.base.exchange,
                    symbol: tickerrc1.symbol,
                    symbol1: tickerrc1.symbol.split('/')[0],
                    symbol2: tickerrc1.symbol.split('/')[1],
                    last: tickerrc1.last,
                    bid: tickerrc1.bid,
                    ask: tickerrc1.ask,
                    [baseFiat]: tickerrc1.last/fiatRate(baseFiat, tickerrc1.symbol.split('/')[1]),
                    [baseCryptocurrency]: -1,
                    fee_exchange: getOurFees(oldOperation.base.exchange)

    }
    let custom_tickerrc2 = {
        exchange: oldOperation.end.exchange,
        symbol: tickerrc2.symbol,
        symbol1: tickerrc2.symbol.split('/')[0],
        symbol2: tickerrc2.symbol.split('/')[1],
        last: tickerrc2.last,
        bid: tickerrc2.bid,
        ask: tickerrc2.ask,
        [baseFiat]: tickerrc2.last/fiatRate(baseFiat, tickerrc2.symbol.split('/')[1]),
        [baseCryptocurrency]: -1,
        fee_exchange: getOurFees(oldOperation.end.exchange)
    }
        
    let refreshedOperation = {
            base: custom_tickerrc1,
            end: custom_tickerrc2,
            baseAsk: custom_tickerrc1.ask/fiatRate(baseFiat, custom_tickerrc1.symbol.split('/')[1]),
            endBid: custom_tickerrc2.bid/fiatRate(baseFiat, custom_tickerrc2.symbol.split('/')[1]),
            gain: (custom_tickerrc2.bid/fiatRate(baseFiat, custom_tickerrc2.symbol.split('/')[1]))
                  /
                  (
                     (custom_tickerrc1.ask/fiatRate(baseFiat, custom_tickerrc1.symbol.split('/')[1]))
                     *  custom_tickerrc2.fee_exchange * custom_tickerrc1.fee_exchange
                    ),
            //baseBuyButton: '<button type="button" onclick="openLoopBuy(this, '+custom_tickerrc1+')">Buy</button>',
            volume: lockedAmountToTrade,
            operationID: getFormattedDate()+"-"+Math.random().toString(36).substr(2, 9)
        };

        //console.log("Pring object custom_tickerrc1..."+JSON.stringify(custom_tickerrc1,null,4));
        //console.log("Pring object custom_tickerrc2..."+JSON.stringify(custom_tickerrc2,null,4));
        //console.log("Pring object refreshedOperation..."+JSON.stringify(refreshedOperation,null,4));
        console.log("Operation Recalculated");
        return refreshedOperation;

}

//GET THE DATA OF ALL THE ORDERS IDS I HAVE IN THE DATABASE
//This function queries the database in search of orders with order_ids, then asks the Exchange the status of that order.
async function getPendingOrders() {

     var sqlQuery_PendingOrders = "SELECT `ord_exchange`, `ord_exchangeorderid` FROM `atm_orders` WHERE `ORD_EXCHANGEORDERID` IS NOT NULL";
     con.query(sqlQuery_PendingOrders, function (err, result, fields) {
                if (err) throw err;
                //console.log("ORDER IDS I HAVE IN THE DATABASE:"+result);
                console.log("CHECK PENDING ORDERS... START");
                for(let j = 0; j < result.length; j++) {
                    //console.log(result[j].ord_exchange + ": " +result[j].ord_exchangeorderid);

                        let checkingexchange = exchanges.find(exchange => exchange.name == result[j].ord_exchange);
                        if(checkingexchange !=  null){
                            if(checkingexchange.hasOwnProperty("has")){
                                if(checkingexchange.has.fetchOrder == true)  { //Check the Order Status IF the exchange has FetchOrder Option
                                    try{
                                           let checkingorder = checkingexchange.fetchOrder(result[j].ord_exchangeorderid);
                                           console.log("Order from "+result[j].ord_exchange+" with ID "+result[j].ord_exchangeorderid+" has the following status: "+ JSON.stringify(checkingorder,null,4));
                                       } catch(error) {
                                           console.log("Error obtaining the CHECK Order Confirmation of "+result[j].ord_exchange+" with ID "+result[j].ord_exchangeorderid+" Error: "+error); 
                                       }
                       }
               else { //IF my Exchange doesnt have the FetchOrder Option dont check it.
                       console.log("Order from "+result[j].ord_exchange + " with ID "+result[j].ord_exchangeorderid+" can't be checked, because this exchange does'nt have FetchOrder Option.");
                   }
                            }
                            
                    }
                        }
                console.log("CHECK PENDING ORDERS... END")
        });
            /*
            let buyOrderConfirmation;
            let sellOrderConfirmation;
            try {
                buyOrderConfirmation = exchangeOperating1.fetchOrder(mybuyorder.id);
                console.log(JSON.stringify(buyOrderConfirmation,null,4));
                } catch(error) {
                                buyOrderConfirmation = "Error obtaining the Buy Order Confirmation: "+error;
                                console.log(buyOrderConfirmation); 
                            }
            try {
                 sellOrderConfirmation = exchangeOperating2.fetchOrder(mysellorder.id);
                 console.log(JSON.stringify(sellOrderConfirmation,null,4));
            } catch(error) {
                             sellOrderConfirmation = "Error obtaining the Sell Order Confirmation: "+error;
                             console.log(sellOrderConfirmation); 
                             }
                            */
}

function getOpenLoops(exchanges, tickers){
    var tickers2 = [];
        for(let baseTicker of tickers){
            for(let endTicker of tickers){
                if(baseTicker.exchange != endTicker.exchange && baseTicker.symbol1 == endTicker.symbol1){
                    let operation = {
                        base: baseTicker,
                        end: endTicker,
                        baseAsk: baseTicker.ask/fiatRate(baseFiat, baseTicker.symbol.split('/')[1]),
                        endBid: endTicker.bid/fiatRate(baseFiat, endTicker.symbol.split('/')[1]),
                        gain: (endTicker.bid/fiatRate(baseFiat, endTicker.symbol.split('/')[1]))
                              /
                              (
                                 (baseTicker.ask/fiatRate(baseFiat, baseTicker.symbol.split('/')[1]))
                                 *  endTicker.fee_exchange * baseTicker.fee_exchange
                                ),
                        //baseBuyButton: '<button type="button" onclick="openLoopBuy(this, '+baseTicker+')">Buy</button>',
                        orders: "N/A", // -1 represents not calculated
                        //Create a unique operationID to identify later on
                        operationID: getFormattedDate()+"-"+Math.random().toString(36).substr(2, 9),
                        atm_evaluation: ""
                    }
                    operation.atm_evaluation = ATM_Evaluate(operation);
                    //stores result of the Evaluation in the database
                    var sqlQuery_EvaluationsInsert = "INSERT INTO `mercury`.`atm_evaluations` (`eval_exchange1`, `eval_exchange2`, `eval_gain`, `eval_result`, `eval_result_reason` ) VALUES ('"+operation.base.exchange+"',  '"+operation.end.exchange+"', '"+operation.gain+"', '"+operation.atm_evaluation[0]+"', '"+operation.atm_evaluation[1]+"' )";
                    con.query(sqlQuery_EvaluationsInsert, function (err, result) {
                        if (err) throw err;
                        //console.log("1 atm_evaluation record inserted. InsertId:"+result.insertId);
                        });
                    //if(operation.atm_evaluation[0]==="APPROVED") operation.orders = await calculateVolume(operation)

                    tickers2.push(operation);
                }
            }
        }
    tickers2.sort(function(a,b) {
        if(a.gain > b.gain) return -1
        return 1
    })
    return tickers2.filter(ticker => ticker.gain > 0)
}

function executeStrategy(openLoops){
    console.log("--- Executing Strategy --- ")
    console.log(openLoops)
    if(openLoops == null){
        console.log("open loops is empty");
        return;
    } 
    openLoops.forEach(async function(operation, index) {
        if(operation.atm_evaluation === "APROVED"){
            //Fetch Order Book
            operation.orders = await calculateVolume(operation)
            openLoops[index].orders = operation.orders
            //Set Volume Limit
            var operatingVolumeLimit;
            switch(openLoops.base.symbol1) {
                case 'BTC': operatingVolumeLimit=0.0025; break;
                case 'BCH': operatingVolumeLimit=0.02; break;
                case 'XRP': operatingVolumeLimit=20; break;
                case 'LTC': operatingVolumeLimit=0.12; break;
                case 'ETH': operatingVolumeLimit=0.03; break;
                default: operatingVolumeLimit='0'
            } 
            let operatingVolume = Math.min(operatingVolumeLimit, operation.orders.volume)
            //SELL
            mercuryLimitOrder(exchanges.find( exchange => exchange.name == operation.endTicker.exchange),
            operation.endTicker.symbol,
            operation.orders.bids[0]*fiatRate(baseFiat, operation.end.symbol.split('/')[1]),
            "sell", operatingVolume)
            //BUY
            mercuryLimitOrder(exchanges.find( exchange => exchange.name == operation.baseTicker.exchange),
            operation.endTicker.symbol,
            operation.orders.bids[0]*fiatRate(baseFiat, operation.base.symbol.split('/')[1]),
            "buy", operatingVolume)
        }
    }, this);
}


async function openLoopBuy(button, ticker){
    console.log("Buying");
    let exchange = exchanges.find(exchange => exchange.name == ticker.exchange);
    await exchange.createLimitBuyOrder (ticker.symbol, 0.0000001, ticker.ask);
    console.log("Sent order");
    button.innerHTML = "Sending order...";
    button.innerHTML = await exchange.fetchOpenOrders (symbol = undefined, since = undefined, limit = undefined, params = {});
    button.onclick = openLoopCancelOrder();
}

async function calculateVolume(operation){
            try{
                let baseExchange = exchanges.find(exchange => exchange.name == operation.base.exchange)
                let baseOrders = await (baseExchange.fetchOrderBook(operation.base.symbol)).catch((error) => {return error})
                let endOrders = await (exchanges.find(exchange => exchange.name == operation.end.exchange).fetchOrderBook(operation.end.symbol)).catch(error =>{return error})
                console.log("Gain ", operation.gain)
                console.log(baseOrders)
                //Convert orders to baseFiat & Remove ask orders that are more than the bidding price
                let baseAsks = baseOrders.asks.map(ask => {ask[0]=ask[0]/fiatRate(baseFiat, operation.base.symbol.split('/')[1]); return ask});
                baseAsks = baseAsks.filter(ask => ask[0]<operation.end.bid/fiatRate(baseFiat, operation.end.symbol.split('/')[1]));
                console.log(endOrders)
                let endBids = endOrders.bids.map(bid => {bid[0]=bid[0]/fiatRate(baseFiat, operation.end.symbol.split('/')[1]); return bid})
                endBids = endBids.filter(bid => bid[0]>operation.base.ask/fiatRate(baseFiat, operation.base.symbol.split('/')[1]))
                if(endBids.length === 0 ) return "No valid bids"
                let totalBidVolume = endBids.map(bid => bid[1]).reduce(function(total, bid){
                    return total + bid
                })
                if(baseAsks.length === 0) return "No valid asks"
                let totalAskVolume = baseAsks.map(ask => ask[1]).reduce(function(total, ask){
                    return total + ask
                })
                console.log("total buy volume ", totalBidVolume)
                console.log("total sell volume: " + totalAskVolume);
                console.log(baseAsks)
                var buyVolume=0;
                var sellProfit =0;
                var buyCost = 0;
                var totalVolume = 0;
                var sellVolume = 0;
                var baseAsksIndex = 0; // Track how many baseAsksAreUsed
                for(let bid of endBids){
                    console.log(bid)
                    sellVolume = bid[1];
                    totalVolume += sellVolume;
                    sellProfit += bid[0]*bid[1];
                    for(let ask of baseAsks){
                        if(ask[0]>=bid[0]) break;
                        if(ask[1]===0) continue;
                        if(sellVolume<=ask[1]){
                            ask[1] -= sellVolume;
                            buyVolume += sellVolume;
                            buyCost += sellVolume * ask[0];
                            sellVolume=0
                            break;
                        }
                        if(sellVolume>ask[1]){
                            buyVolume += ask[1];
                            buyCost += ask[1] * ask[0];
                            sellVolume -= ask[1];
                            ask[1]=0;
                        }
                    }
                    if(sellVolume>0){
                        sellProfit -= sellVolume*bid[0];
                        totalVolume -= sellVolume
                        break;
                    }
                }
                let profit = sellProfit - buyCost;
                console.log("SellVolume "+ totalVolume);
                console.log("buyVolume "+ buyVolume)
                console.log("buyCost: " + buyCost);
                console.log("sellProfit :"+ sellProfit)
                console.log("Profit: "+ profit);
                return {volume: totalVolume, buyCost: buyCost, profit: sellProfit - buyCost, gain: (buyCost+profit)/buyCost, bids:endBids, asks: baseAsks};
            }
            catch(error){
                console.log(error);
                return "error";
            }
}

//Returns Current Timestamp if format: 2018-02-28 h:m:s
function getFormattedDate() {
    var date = new Date();
    var str = date.getFullYear() + "-" + ( "0"+(date.getMonth() + 1)).slice(-2) + "-" +("0" + date.getDate()).slice(-2) + "-" +  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    return str;
}

function calculateClosedLoopGain(baseTicker, middleTicker, endTicker){
    let basePrice = baseTicker.bid;
    let middlePrice = middleTicker.ask;
    let endPrice = endTicker.bid;
    let baseFee = baseTicker.fee_exchange;
    let middleFee = middleTicker.fee_exchange;
    let endFee = endTicker.fee_exchange;
    if(baseTicker.symbol1 != middleTicker.symbol1 || baseTicker.symbol2 != middleTicker.symbol2)  middlePrice = 1/middlePrice
    if(baseTicker.symbol2 != endTicker.symbol2 || baseTicker.symbol1 != endTicker.symbol1) endPrice = 1/endPrice
    if(baseTicker.symbol1 == middleTicker.symbol1)
        return ((middlePrice*endPrice-basePrice)/baseTicker.last) / (baseFee * middleFee * endFee)
    else return ((middlePrice*endPrice-basePrice)*baseTicker.last)  / (baseFee * middleFee * endFee)
}

function getClosedLoops(exchanges, tickers){
    var closedLoops = [];
    for (let exchange of exchanges){
        for(let baseTicker of tickers){
            for(let middleTicker of tickers){
                for(let endTicker of tickers){
                    if(baseTicker.exchange != middleTicker.exchange && middleTicker.exchange != endTicker.exchange && endTicker.exchange == baseTicker.exchange && baseTicker.symbol != middleTicker.symbol){
                        if(endTicker.symbol.includes(baseTicker.symbol1) && (middleTicker.symbol.includes(baseTicker.symbol2)) || (endTicker.symbol.includes(baseTicker.symbol2) && middleTicker.symbol.includes(baseTicker.symbol1))){
                            if((baseTicker.symbol.includes(middleTicker.symbol1) && endTicker.symbol.includes(middleTicker.symbol2)) || ((baseTicker.symbol.includes(middleTicker.symbol2) && endTicker.symbol.includes(middleTicker.symbol1)))){
                                if((middleTicker.symbol.includes(endTicker.symbol1) && baseTicker.symbol.includes(endTicker.symbol2)) || (middleTicker.symbol.includes(endTicker.symbol2)&& baseTicker.symbol.includes(endTicker.symbol1))){
                                    let operation = {
                                        base: baseTicker,
                                        middle: middleTicker,
                                        end: endTicker,
                                        gain: calculateClosedLoopGain(baseTicker,middleTicker, endTicker),
                                        buyButton: '<button type="button">Buy</button>'
                                        //[baseFiat]: calculateClosedLoopGain(baseTicker,middleTicker, endTicker)/fiatRate(baseFiat, endTicker.symbol2)
                                    }
                                    closedLoops.push(operation);
                                }
                            }
                        }
                    } 
                }
            }
        }
    }
    closedLoops.sort(function(a,b) {
        if(a.gain > b.gain) return -1;
        return 1;
    })
    return closedLoops;
}


var openLoops = [];

(async () => {
    console.log("--------------MERCURY GETTING STARTED --------------------")
    //Get FIAT DATA & Test
   /*
    updateFIATData(baseFiat);
    console.log("<--- Loaded Fiat exchange rates --->")
    console.log("Rate from EUR to AUD");
    console.log(fiatRate("EUR", "AUD"));
*/


    console.log("Trying to update EUR-FIAT exchange rates...");
    await updateEURFIATData(); //THIS FUNCTION SHOULD FINISH BEFORE IT CONTINUES BUT DOESNT!!!!!!
    setInterval(updateEURFIATData, fiatDataRefreshRate*1000);

    //console.log("Rate from EUR to AUD: "+fiatRate("EUR", "AUD"));
    console.log("fiatData es:"+fiatData);
    console.log("The euro/dolar is at: "+fiatData.rates["USD"]);
    console.log(fiatRate("EUR", "AUD"));
    console.log("Finished updated FIAT Exhange rates");
    

    var ratesHtml = tableify(fiatData.rates);


    //I  print out the current fees of brokers like this
    console.log("BROKER FEES:");
    for(var i=0; i<ourfees.length; i++){
                   console.log("FEE: "+
                               ourfees[i].name
                               + " " +
                               ourfees[i].operation
                               + " " +
                               ourfees[i].fee
                           );
    }
    //I can get the fee of certain operation of a broker like this
    //console.log ( (ourfees.find(o => o.name ==='btcmarkets' && o.operation==='ALL')).fee );

exchanges.push(myexchanges.bitstamp);
exchanges.push(myexchanges.btcmarkets);
//exchanges.push(myexchanges.acxio);
exchanges.push(myexchanges.kraken);
exchanges.push(myexchanges.luno);
exchanges.push(myexchanges.coinspot);
exchanges.push(myexchanges.coinmate);
//exchanges.push(myexchanges.gatecoin);
//exchanges.push(myexchanges.dsx);

    // LOAD MARKETS
    for (var i=0;i<exchanges.length;i++){
        //Load available Markets in the broker
        await(exchanges[i].loadMarkets()).catch((error)=>{return error})
    }

    /*********
     * WEB SERVER SECTION
     ********/
    const express = require('express');
    const app = express();
    var basicAuth = require('express-basic-auth');
    app.use(basicAuth({
        users: { 'mercury': 'mercurypass' },
        challenge: true,
        }));

    //LOAD WEBSITE
   var htmlheader =  fs.readFileSync('original/html/header.html');

    //Fetch Balance
    balances = await getBalances(exchanges).catch((error)=>{return error})
   var balanceHttp = tableify(balances)
   setInterval(async function(){
    balances = await getBalances(exchanges).catch((error)=>{return error})
    var balanceHttp = tableify(balances)
    app.get('/balances', (req, res) => res.send(htmlheader + balanceHttp));
    console.log("<---- REFRESHED BALANCES --->");
   }, balancesRefreshRate*1000)
   console.log("<---- LOADED BALANCES --->");

   //Show Fees
   var feesHttp = tableify(ourfees);
   app.get('/fees', (req, res) => res.send(htmlheader+feesHttp));
   var tradeHtml;

    //Fetch Data
   var tickers, openLoopHtml, closedLoopHtml;
   tickers = await fetchData(exchanges);
  
   openLoops = getOpenLoops(exchanges, tickers);
   console.log("<---- LOADED "+tickers.length+" TICKERS ---->")
   setInterval(async function (){
            tickers = await fetchData(exchanges);
            openLoops = getOpenLoops(exchanges, tickers);    
            console.log("<---- LOADED "+tickers.length+" TICKERS ---->")
            closedLoopHtml = tableify(getClosedLoops(exchanges, tickers));
            app.get('/closedloop', (req, res) => res.send(htmlheader+closedLoopHtml));
            
   }, tickersRefreshRate*1000)
   
    //Refresh OpenLoopsView
    openLoopHtml = tableify(openLoops);
    app.get('/', (req, res) => res.send(htmlheader + openLoopHtml));
    setInterval(function(){
        openLoops = getOpenLoops(exchanges, tickers);        
        openLoopHtml = tableify(openLoops);
         app.get('/', (req, res) => res.send(htmlheader + openLoopHtml));
      }, analysisViewRefreshRate*1000)
        
   closedLoopHtml = tableify(getClosedLoops(exchanges, tickers));
   app.get('/closedloop', (req, res) => res.send(htmlheader+closedLoopHtml));
   app.get('/openorders', (req,res) => res.send(htmlheader + fs.readFileSync('original/html/openorders.html'))); 
   app.get('/test', (req, res) => res.send(htmlheader + fs.readFileSync('original/html/trade.html') ));

   //debug_show_exchange_information();

   //Execute Strategy
   console.log(openLoops)
   if(trading) execexecuteStrategy(openLoops);
   setInterval(function(){
       if(trading){
        console.log("<--- Executing Strategy --->");       
        executeStrategy(openLoops);
       }
   }, executeStrategyRefreshRate*1000)

   app.get('/trade', (req, res) => res.send("Trading: "+ trading));
   app.post('/trade', function(req, res) {
    trading = !trading;
    res.send("Trading: "+ trading);
    app.get('/trade', (req, res) => res.send("Trading: "+ trading));
});


   //REFRESH DATA 
   setInterval(function(){
        console.log("REFRESHING...");
      //Output TIMESTAMP
      console.log("TIMESTAMP: "+getFormattedDate());
        //Empty my array. This is perfect if you don't have references to the original array A anywhere else because this actually creates a brand new (empty) array. You should be careful with this method because if you have referenced this array from another variable or property, the original array will remain unchanged. Only use this if you only reference the array by its original variable A.
        ATM_Operations_In_Queue = [];
        app.get('/openorders', (req,res) => res.send(htmlheader + fs.readFileSync('original/html/openorders.html')));
        app.get('/test', (req, res) => res.send(htmlheader + fs.readFileSync('original/html/trade.html') ));
    }, refreshRate*1000)

    //LOAD WEB SERVER
   //Try to load SSL if not launch without (for running app localy)
    if(ssl) {
        console.log('Starting SSL Server...');
        https.createServer(ssl_options, app).listen(3000);
        console.log('Mercury app listening on port 3000')
    }
    else{
        console.log('SSL not active. Starting HTTP Server...')
        app.listen(3000, () => console.log('Mercury app listening on port 3000!'));
    }

    //Now that I have all the Candidate Operationes I prioritize them and execute the best one.
    //await ATM_Prioritize();

    //Get PendingOrders
    //await getPendingOrders();

}) ()
