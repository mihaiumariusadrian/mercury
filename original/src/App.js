import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ArbitradgeTable from './ArbitradgeTable.js'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to Criadoperez Crypto corner</h1>
        </header>
        <p className="App-intro">
          <ArbitradgeTable />
        </p>
      </div>
    );
  }
}

export default App;
